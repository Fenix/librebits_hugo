---
title: Acerca de mi
---


Mi nombre es Fenix, un desarrollador web en Madrid. Este sitio web [es un tributo a internet](/blog/a-tribute-to-the-web) que tomo prestado de [la web de Usecue](https://usecue.com). 

Estudié Imagen, Sonido, Telecomunicaciones, Informática y Electrónica en Barcelona. En 2012 fundé 'Librebits'.

Uso herramientas como Jekyll y Hugo (generadores web estáticos website). Colaboro en el desarrollo de [Hugo Codex](https://hugocodex.org).

&nbsp;

<p style="text-align: center;"><img src="/img/fenix-caricatura.jpeg" style="max-width: 100%; width: 400px;" alt="Fenix" /><br /><em>Yo, visto por un amigo.</em></p>
