---
title: Hugo Codex Es
image: /uploads/hugocodex1.png
mobile_image: /uploads/hugocodex2.png
color: '#515151'
portfolio_url: 'https://www.hugocodex.org/es'
google_score: 98%
speed_index: 1.5s
page_weight: 264kb
order: 1
---

Colaboro en el desarrollo de este sitio web que nos facilita un lugar en el que disponer de nuestros snippets de
código fuente  y las mejores prácticas. 

He evolucionado desde la construcción de sitios web con Jekyll (basado en el lenguagje de programación ruby)  a construirlos con el framework Hugo (basado en golang). 

El sitio es enteramemente código libre. Incluso contiene una simple implementación de una tienda online. No sólo desarrollamos este sitio web
para acomodar nuestro flujo de trabajo, ademaś lo albergamos en nuestro 
[perfecto gestor de contenidos (CMS) para Hugo](https://cms.usecue.com)!
