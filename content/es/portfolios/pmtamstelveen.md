---
title: PMT Amstelveen
image: /uploads/pmtamstelveen1.png
mobile_image: /uploads/pmtamstelveen2.png
color: '#519151'
portfolio_url: 'https://www.pmtamstelveen.nl/'
google_score: 97%
speed_index: 1.6s
page_weight: 289kb
order: 2
---

Realizé transformación de Jekyll a Hugo de este sitio web. Lo albergamos en nuestro 
[perfecto gestor de contenidos (CMS) para Hugo](https://cms.usecue.com)!
