---
title: Smitwoninginrichting
image: /uploads/smitwoningrichting1.jpg
mobile_image: /uploads/smitwoningrichting2.jpg
color: '#515391'
portfolio_url: 'https://www.smitwoninginrichting.nl/'
google_score: 78%
speed_index: 1.7s
page_weight: 360kb
order: 2
---

Realizamos la migración de Jekyll a Hugo. En esta ocasión, respetamos el código fuente css en su framework original.  Ademaś lo albergamos en nuestro 
[perfecto gestor de contenidos (CMS) para Hugo](https://cms.usecue.com)!
