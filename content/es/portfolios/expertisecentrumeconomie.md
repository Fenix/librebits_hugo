---
title: Expertisecentrum Economie en Handel
image: /uploads/ece1.png
mobile_image: /uploads/ece2.png
color: '#ac1e40'
portfolio_url: 'https://www.expertisecentrumeconomie.nl/'
google_score: 92%
speed_index: 1.5s
page_weight: 389kb
order: 3
---

Realizamos la migración de Jekyll a Hugo. En  este proyecto, no usamos para el diseño más que código fuente css 'vainilla'.

Ademaś lo albergamos en nuestro  [perfecto gestor de contenidos (CMS) para Hugo](https://cms.usecue.com)!

