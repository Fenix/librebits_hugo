---
title: NOCK 
image: /uploads/nock1.png
mobile_image: /uploads/nock2.png
color: '#b1901'
portfolio_url: 'https://koudenhoornontdekken.nl/'
google_score: 97%
speed_index: 1.6s
page_weight: 289kb
order: 5
---

Realizamos la migración de Jekyll a Hugo. En  este proyecto, no usamos para el diseño más que código fuente css 'vainilla'.

Ademaś lo albergamos en nuestro  [perfecto gestor de contenidos (CMS) para Hugo](https://cms.usecue.com)!


