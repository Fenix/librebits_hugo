---
title: Google Analytics binnenkort verboden?
---

'Let op: gebruik Google Analytics mogelijk binnenkort niet toegestaan', las ik op [de website van De Authoriteit Persoonsgegevens](https://web.archive.org/web/20220114173118/https://autoriteitpersoonsgegevens.nl/nl/onderwerpen/internet-telefoon-tv-en-post/cookies). Wat!? Dat is andere taal dan ik gewend was van de AP.

De tekst ging verder: De Oostenrijkse privacytoezichthouder rondde in januari 2022 een onderzoek af naar het gebruik van Google Analytics door een Oostenrijkse website. Google Analytics blijkt volgens de Oostenrijkse toezichthouder in dit onderzochte geval niet aan de AVG te voldoen. De AP onderzoekt op dit moment twee klachten over het gebruik van Google Analytics in Nederland. Na afronding van dat onderzoek, begin 2022, kan de AP zeggen of Google Analytics nu is toegestaan of niet.

Hier staat niet 'dat je Google Analytics privacy vriendelijk moet instellen'. Hier staat niet 'dat Google Analytics verantwoord kan worden door middel van de noodzaak ter behartiging van uw gerechtvaardigd belang'. Hier staat gewoon *letterlijk* dat de AP begin 2022 kan zeggen of 'Google Analytics nu is toegestaan of niet'. 

En dat is wel heel vreemd, want hoewel ik niet kan zeggen dat ik het onterecht vindt, is dit wel schokkend ongenuanceerd. Het lijkt erop dat de AP hier voorsorteert op twee mogelijkheden: het mag wel of het mag niet. Vooral het woordje 'nu' vind ik opmerkelijk. Het kan een concluderende 'nu' zijn, maar het kan ook verwijzen naar 'op dit moment'. Ik hoop toch dat ze hier het laatste bedoelen. Maar zelfs dan sorteren ze hier voor op het verbieden van Google Analytics in zijn geheel (zo lijkt het).

Stel je voor dat de AP binnen enkele weken met de uitspraak komt dat het gebruik van Google Analytics vanaf dat moment *in zijn geheel* verboden is... Stel je voor.