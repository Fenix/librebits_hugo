---
title: Een nieuw algoritme voor social media
---

Ergens halverwege mijn studie (zo rond het jaar 2002) zat ik in de websitecommissie van de studievereniging ID (Industrial Design) in Delft. De discussie die we op dat moment voerden ging over een nieuw concept voor de website van onze vereniging. 

## Een extreem idee

Het idee was om de inhoud van de hele website 'rate'-baar te maken. Dat idee hadden we geleend van een op dat moment populaire en spraakmakende website: 'Rate my face', waarop je gezichten van echte mensen een cijfer kon geven. Je kon het hoofd van je buurman of -vrouw uploaden en dan kon iedereen op internet dat een cijfer geven. Wij vonden het grensoverschrijdend, maar we smulden ervan! Voor de duidelijkheid: wij waren een groep twintigers (mannen) die elkaar met 'lul' aanspraken en hard lachten om de harde en lompe grappen van Hans Teeuwen. Voor subtiliteit moest je niet bij ons zijn.

Op een commissievergadering op woensdagmiddag zaten we met vijf studenten in een achterkamertje van het ID Kaffee. Wij deden ons tegoed aan bier en pizza, terwijl op de achtergrond de meters bier over de bar vlogen. Daar, op dat moment, presenteerde ik dit idee. Je moest niet alleen profielen, maar ook nieuwsberichten, foto's en commentaren een cijfer kunnen geven. Het idee werd afgeschoten door mijn medestudenten. De virtuele status die dit principe zou opleveren en de combinatie van 'anonimiteit' en 'beoordelen' zou een grote aanjager van intolerantie en slechte sfeer zijn. Ik kon niet anders dan ze gelijk geven.

Grappig genoeg liet Mark zich, aan de andere kant van de planeet, op ongeveer datzelfde moment, niet tegenhouden door dit soort morele bezwaren. Hij ontwikkelde het platform Facebook met een vergelijkbaar concept. Het werd een wereldwijd begrip. Ook Twitter, waar anonimiteit een nog grotere rol speelt, werd een groot succes. Je hoorde aanvankelijk niemand over de door ons voorziene 'intolerantie', 'slechte sfeer' en 'virtuele status'.

## Fast forward naar nu

We gebruiken nu al 20 jaar social media, waarin we alles van elkaar beoordelen. En als je mij (of andere mensen) vraagt om drie problemen te noemen van deze sociale netwerken, dan zijn het wel 'intolerantie', 'slechte sfeer' en 'het najagen van virtuele status'. Mensen maken constant ruzie op social media en hun tolerantie voor mensen met een andere mening heeft een absoluut dieptepunt bereikt. Feiten die mensen niet aanstaan? Die verwoorden ze als meningen en vallen ze aan (met veel succes). We zien dat niet alleen het koopgedrag van mensen wordt beïnvloed via social media, maar ook waar ze in geloven ('triviale' dingen zoals wetenschap) en wat ze kiezen tijdens landelijke verkiezingen.

[Drie jaar geleden](/nl/blog/internet-en-de-boekdrukkunst/) schreef ik: "Dat Facebook en Twitter slechte ideeën waren zijn we het inmiddels wel over eens toch!?" Gek genoeg lijkt het van niet. Het is 2022 en beide platformen bestaan nog en hebben nog steeds miljoenen, nee, miljarden gebruikers. Ik word er moedeloos van, zeker omdat we de problemen die ze opleveren al 20 jaar geleden voorzagen.

## Hoe social media nu werken

Aangezien ik het 'allemaal zo goed weet', heb ik een nieuw algoritme bedacht voor social media. Maar voordat ik dat kan uitleggen moet ik nog even kort uitleggen waarom de huidige algoritmes niet goed werken. Dat kan ik het beste doen aan de hand van een tweetal anekdotes.

*"Ik zag een dronken man op een stoep, met een fles in zijn hand, eenzaam door een grote straat in Amsterdam strompelen. Het regende. Zijn vrouw had hem verlaten en hij brulde zo hard hij kon: 'Alle vrouwen zijn kuthoeren!' Het weergalmde tussen de huizen. De straat was vrijwel verlaten en een vrouw die hem tegemoet kwam stak over, zodat ze hem niet dichtbij hoefde te passeren. Met een combinatie van medelijden en afschuw keek ze de man na. Aan het einde van de straat zag ze een politieagent de beste man aanspreken en in een busje zetten. De agent vroeg hem naar zijn naam en zijn adres. De agent zou hem thuis afzetten, of, als de man niet meewerkte, een nachtje zijn roes laten uitslapen in de politiecel. De man was eenzaam, machteloos en de draad kwijt. Hij was onzichtbaar en nietig en de agent was zijn onzichtbare vriend.”*

*"Ik zag ook een dronken man op de digitale snelweg. Zijn vrouw had hem verlaten en hij schreef in een tweet: 'Alle vrouwen zijn kuthoeren!' Aanvankelijk gebeurde er niets, maar al snel had zijn tweet honderden reacties. Vanaf dat moment ging het snel. Er ontstond tumult. De man, die zich zo eenzaam voelde, stond in het midden van alle aandacht. Steeds meer mensen gingen zich ermee bemoeien. De politie was in geen velden of wegen te bekennen. Hackers probeerden de identiteit van de man te achterhalen. Hij werd aan alle kanten verbaal aangevallen. Hij probeerde zich te verdedigen, maar dat maakte het alleen maar erger. Ondanks zijn eenzaamheid voelde hij zich het centrum van het universum. Jammergenoeg een universum zonder tolerantie, zonder begrip en met een slechte sfeer. Een universum waarin iedereen zijn 'morele superioriteit' lijkt te willen onderstrepen, zelfs tegenover een man op het laagste punt van zijn leven.”*

Voel je het verschil en herken je het gedrag?

## Het nieuwe algoritme

Neem het voorbeeld van de dronken man en je ziet dat online de context ontbreekt. Daardoor kun je geen (fatsoenlijke) discussie voeren of compassie tonen.... zeker niet met maar 140 tekens. De wereld is nou eenmaal nooit zwart-wit. Zelfs niet wanneer iemand zoiets aanstootgevends roept als de man in het voorbeeld. In huidige algoritmes wordt controverse beloond met aandacht en daar moeten we simpelweg mee stoppen. 

Ik ben inmiddels geen lompe Delftse student meer, maar een vader van twee kinderen. Wat me opvalt is dat discussies op social media lijken op hoe (en waarom) kinderen ruzie maken.

> Wat me opvalt is dat discussies op social media lijken op hoe (en waarom) kinderen ruzie maken.

Kinderen schreeuwen, trekken daarmee de aandacht en zien de wereld zwart-wit. Ze hebben een enorm rechtvaardigheidsgevoel en kunnen zich niet goed inleven in de gevoelens van een ander. Door hun gebrek aan levenservaring kennen ze geen verzachtende omstandigheden. Ook kunnen ze de persoon niet los zien van het onderwerp. Hierdoor vellen kinderen vaak een meedogenloos oordeel. 

Precies datzelfde zie ik op social media, alleen is daar niet het gebrek aan levenservaring of hersenontwikkeling, maar het gebrek aan context de oorzaak van de intolerantie. Het willen onderstrepen van morele superioriteit komt niet voort uit een aangeboren rechtvaardigheidsgevoel, maar het wordt gevoed door de likes en follows van gelijkgestemden, ofwel: virtuele status. Dat principe werkt het zwart-wit denken op twee manieren in de hand: het beloont extreme standpunten en het veroorzaakt een bubbel. Kortom: discussies op social media worden hoe langer hoe kinderlijker.

Ik hoor je denken: maar heeft het belonen van controverse niet ook geleid tot de democratisering van journalistiek en voor de Arabische lente? Ja, misschien... maar nu fake nieuws en deep-fake video's in opkomst zijn, vrees ik dat dat effect wel 'op' is. Weloverwogen, verhelderende en niet polariserende berichten zouden een verademing zijn. Gebrek aan controverse zou het criterium moeten zijn voor het vergroten van het bereik van een bericht.

> Gebrek aan controverse zou het criterium moeten zijn voor het vergroten van het bereik van een bericht. 

Er is een beroemde tweet, met alleen het woord 'No' erin, vergelijkbaar met een kind dat 'Nietes' roept. Dat zou niet veel bereik hebben met dit nieuwe algoritme. Commerciële uiting? Beetje controversieel, dus niet veel bereik. Poes zoek? Veel bereik. Mooie vakantiefoto? Ook redelijk veel bereik. Politiek standpunt? Erg controversieel, dus nauwelijks bereik.

Social media wordt met dit nieuwe algoritme een soort familiebezoek. Daar ga je ook niet proberen om een stofzuiger aan je oom te verkopen en zal je een verhitte discussies over politiek proberen te vermijden. Zo zou dat op social media ook moeten zijn. Heette dat niet '[netiquette](https://www.ietf.org/rfc/rfc1855.txt)', 30 jaar geleden?

Een bereik beperkend controversefilter zou trouwens ook goed zijn voor sollicitatiegesprekken. Die beschamende controversiële post zou allang door een onzichtbare vriend zijn 'verwijderd'. Het zou zorgen dat je op je goede dagen niet door je slechte wordt achtervolgd. Heerlijk.

## Een nieuw platform

Mark, heb jij kinderen? Herken je het gedrag? Ik help je graag om een volwassen social media platform te maken. Desnoods noemen we het 'the metaverse'. En geef toe... je hebt nu snel iets nieuws nodig, want Facebook is 'over the hill', zoals ze bij jullie zeggen. Jack en Parag van Twitter, voor jullie geldt (uiteraard) hetzelfde.
