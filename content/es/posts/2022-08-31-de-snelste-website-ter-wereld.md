---
title: De snelste website ter wereld
---

Deze website zou best wel eens [de snelst ladende website ter wereld](/blog/websites-that-load-instantly) kunnen zijn. Is hij echt snel? Zeker! Check het zelf maar op https://tools.pingdom.com. Ik denk dat je uitkomt op een een laadtijd van onder de 150 milliseconden. Indrukwekkend, nietwaar? 

Toen ik zocht op 'fastest website in the world' vond ik [Robin J.E. Scott](https://robinjescott.com/fastest-website-world/). Hij suggereert dat hij de snelste website ter wereld heeft, maar zijn website laadt twee keer zo traag als die van mij (245 milliseconden). 

Veel mensen houden van [snelle auto's](https://hugocodex.org/blog/image-compression-for-the-lazy/). Ik houd meer van [snelle websites](/blog/websites-that-load-instantly). Waarom? Allereerst omdat je [bijna](/blog/monkey-business-with-banana-leafs) niet vals kunt spelen in 150 milliseconden... ten tweede omdat [onderzoek heeft uitgewezen](/blog/the-need-for-speed) dat een snelle website VEEL meer geld oplevert... ten derde omdat snelheid een maatstaf is voor kwaliteit... en tenslotte omdat Google [in 2008](/blog/websites-that-load-instantly) met de Google Chrome browser al aantoonde dat mensen het fantastisch vinden als een website 'instant' laadt.

Natuurlijk maakt de geografische locatie van je bezoeker (en je server) enorm veel uit bij de snelheid van je website. Mijn 'supersnelle' website laadt 5x zo traag (in ongeveer 650 milliseconden) [vanuit Vancouver in Canada](https://gtmetrix.com/). Maar tegelijk is die laadtijd van 650 milliseconden nog steeds VEEL lager dan de laadtijd van de meeste websites. Ik zie CDN's dan ook als [een doekje voor het bloeden](https://www.ensie.nl/betekenis/een-doekje-voor-het-bloeden). Slechte sites lijken er minder slecht door en goede sites hebben het niet nodig. De latency die veroorzaakt wordt door een CDN is vaak behoorlijk, terwijl een request de halve wereld rond kan gaan in 500 milliseconden. En dan te bedenken dat een CDN ook 'cache misses' heeft... Oef!

Ik vind een laadtijd van 150 milliseconden, plus eventueel 500 milliseconden voor een geografisch ongunstig request, heel acceptabel. En ja... ik hoop dat ik (in ieder geval vanuit West-Europa) de snelst ladende website ter wereld heb.

Maargoed... het gaat niet alleen om de laadtijd. Hoe snel kun je de website updaten? De meeste snelle websites zijn gemaakt met een 'Static Site Generator' (SSG). Hoewel je hiermee razendsnel ladende website kunt produceren, duurt het live zetten van een update vaak enkele minuten(!). Dat is het tegenovergestelde van een snelle website. Deze website, die ook met een SSG is gebouwd, doet dat echter in [minder dan een seconde](/blog/oh-jamstack-grow-up). Hoewel dat standaard is voor vrijwel iedere WordPress website, is dat het zeker niet in SSG-land. Tot nu toe is het namelijk alleen [Kyle Mathews](/blog/gatsby-has-grown-up/) gelukt om zijn SSG website binnen een seconde live te zetten.

Kortom, ik denk... en hoop... dat deze website (op dit moment) echt de snelste website ter wereld is.
