---
title: Een nieuw Jamstack tijdperk
---

In 2015 ben ik begonnen met het bouwen van Jamstack websites. In de afgelopen 7 jaar heb ik nauwelijks downtime gehad. Ik heb 250 websites gebouwd. De snelheid van de websites en van het CMS was best goed, zeker als je het vergelijkt met WordPress op shared hosting. Daarnaast heb ik relatief weinig onderhoud gehad aan de sites en is er nooit eentje gehackt of besmet. 

Niets aan doen, zou je denken. Maar er waren ook wel problemen. Zo was er geregeld iets stuk in het CMS en ik heb meer dan eens de formulierverzendingen moeten verhuizen vanwege problemen met de leverancier. Ook kwam ik erachter dat het allemaal nog veel sneller kon: zowel de hosting als het updaten van de website. Daarnaast was de hele oplossing niet erg privacyvriendelijk. Maar de druppel was dat mijn leverancier zijn prijzen verhoogde.

## Hugo 

Ik bouwde al mijn websites in Jekyll. Jekyll was een veilige en makkelijke keuze. Het was in 2015 de grootste Static Site Generator. Ik werd zo goed in het bouwen van Jekyll websites dat ik op JekyllConf 2019 heb laten zien dat ik een Jekyll website met CMS en form builder kon bouwen in minder dan 20 minuten. Toch heb ik er voor gekozen om over te stappen naar Hugo. Hugo is moeilijker om te leren, maar biedt wel een heel groot voordeel, namelijk: ultieme snelheid. Wanneer je in een Jekyll website een wijziging doorvoert moet je soms tot wel 5 minuten wachten voordat je wijziging online staat. Dat is met de keuze voor Hugo verleden tijd. Vrijwel elke wijziging staat binnen een seconde live. Daarnaast kan Hugo ook afbeeldingen comprimeren, iets dat heel belangrijk is bij het optimaliseren van websites.

## Een eigen CMS

Als CMS gebruikte ik al sinds 2015 CloudCannon. CloudCannon ondersteunde aanvankelijk ook alleen maar Jekyll. CloudCannon is snel, prettig in gebruik en het heeft superveel features. Recent is het echter 10 keer zo duur geworden. Het is CloudCannon kennelijk niet gelukt om voldoende durfkapitaal aan te trekken en/of om snel genoeg te groeien. Mijn kosten zouden ongeveer duizend euro per maand worden. Serieus geld, maar ook gelijk een kans om zelf mijn ideale CMS te ontwikkelen: technisch hoogstaand en tegelijk eenvoudig in gebruik. Het is gelukt en ik heb het Usecue CMS genoemd. Het CMS is te vinden op https://cms.usecue.com. Inmiddels staan de eerste 20 sites in het CMS.

## Eigen servers

Ik gebruikte vaak de hosting van CloudCannon, maar zei hostten hun websites op Cloudflare. Er zijn veel redenen om je website niet op Cloudflare (of een ander CDN) te hosten, maar snelheid is zeker een belangrijke. Nu ik mijn websites op mijn eigen servers heb staan zijn ze tot wel 10x zo snel geworden. Dit heeft te maken met betere hardware, maar ook met een veel slimmere caching strategie.

## Eigen formulierafhandeling

Uit gemak gebruikte ik vaak de formulierafhandeling van een derde partij, zoals bijvoorbeeld het Amerikaanse Formbucket of het formulierensysteem van CloudCannon. Hiervoor moest de invuller van het formulier echter expliciete toestemming geven vanwege de AVG. De persoonsgegevens werden immers naar Amerika gestuurd en daar opgeslagen. Dat was niet erg professioneel. Daarnaast was er meer dan eens downtime, wat soms zelfs inkomstenverlies betekende voor klanten. Daarom heb ik mijn eigen software gebouwd, die ik heb geïntegreerd in het CMS.

## Statistiek via log analyse

Google Analytics is een mooie tool, maar het wordt steeds lastiger om deze conform de richtlijnen van de AVG te gebruiken. Daarom heb ik gekozen om (net als Netlify, CloudCannon en andere grote partijen) over te stappen op log analyse. Het nadeel is dat je minder over je bezoekers te weten komt, maar het voordeel is dat de cijfers veel betrouwbaarder zijn. Deze log analyse heb ik geïntegreerd in het CMS, zodat je alle informatie handig bij elkaar hebt.

## Concluderend

Ongelooflijk veel verbeteringen, mede mogelijk gemaakt door de gigantische kostenstijging van CloudCannon. Natuurlijk is het niet goedkoop om alles zelf te bouwen, maar het levert je wel een veel hoger serviceniveau op. Bovendien ben ik de eerste die een website in 0,1 seconde op het scherm kan toveren en hem in minder dan een seconde kan updaten. 

Kortom: ik bouw privacyvriendelijke websites, die alles kunnen wat je wil en die sneller zijn dan welke andere website dan ook. Is dat ook wat voor jou? Neem dan contact op.