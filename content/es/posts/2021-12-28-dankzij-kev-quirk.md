---
title: Dankzij Kev Quirk
---

Ik kreeg een e-mail van [Kev Quirk](https://kevq.uk). Hij wees mij erop dat mijn website geen RSS feed had, iets dat hij kennelijk [vaker doet](https://kevq.uk/please-add-rss-support-to-your-site/). Uiteraard heb ik direct een RSS feed toegevoegd. We wisselden een paar mailtjes uit en ik raakte gefascineerd door zijn blog.

Dankzij Kev Quirk:

- heb ik nu een [RRS feed](https://www.usecue.com/nl/index.xml) op mijn website
- ben ik lid van de [512kb club](https://512kb.club/)
- weet ik dat 300 euro per jaar een [redelijk](https://kevq.uk/goodbye-wordpress-switched-to-jekyll/) bedrag is voor WP hosting
- heb ik een paar toffe nieuwe [blogs](https://kevq.uk/blogroll/) en [podcasts](https://kevq.uk/blogroll/) ontdekt
- werd ik geinspireerd om een 'dingen die ik dagelijk gebruik' lijst te maken (zie hieronder)
- wil ik graag meer weten over [Fosstadon](https://fosstodon.org/@kev)
- had ik lol met zijn [Weird Wide Web Ring](https://weirdwidewebring.net/)
- heb ik nu een vermelding op zijn [blogroll](https://kevq.uk/blogroll/)

Dingen die ik dagelijk gebruik:

- Linux
- Visual Studio Code
- Firefox
- Hugo
- Inkscape
- Gimp
- disk encryption
- bedrade computer accessoires, zoals een conference speakerphone en een mechanisch toetsenbord

Dus... dankjewel Kev!