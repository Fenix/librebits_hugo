---
title: Onvolwassen vakgebieden
---
Heb je wel eens nagedacht over wat een tuinman van een tuincentrum vindt, waar mensen zomaar zelf hun plantjes kunnen uitzoeken? En wat een kunstenaar van een hobbywinkel vindt? Of hoe een timmerman aankijkt tegen de zaag in jouw gereedschapskist? 

Ik wel. Ze denken: Wat leuk, meer enthousiastelingen!

Zo gaat dat niet in mijn vakgebied: website development. Daar kijken de professionals vaak met gruwen naar doe-het-zelf website software. En dat steken zij ook niet onder stoelen of banken. Ze voelen zich kennelijk bedreigd. Vooral Wix heeft het zwaar te verduren. Wix is een tool waarmee je met gemak je eigen website kunt ontwerpen en bouwen. Ideaal voor de doe-het-zelver dus. Maar Wix is slecht en de vijand, tenminste, als ik mijn vakgenoten mag geloven. Datzelfde geldt overigens in mindere mate voor Wordpress, dat zojuist 'full site editing' heeft geïntroduceerd. Het is sinds kort mogelijk om het design van je hele website aan te passen met dit *content* management systeem. Handig toch? Zeker handig voor een doe-het-zelver... Misschien overbodig om te zeggen, maar een professional zal het beheer van het design en het beheer van de content liever gescheiden houden.

En er is niet alleen afschuw. Er zijn ook bedrijven die serieuze software bouwen en die (ongemerkt) opschuiven richting dit doe-het-zelf gebeuren. Wordpress (Automattic) is er daar dus eentje van, maar ook Forestry en CloudCannon doen hieraan mee. Toch lijkt het niet alsof deze bedrijven inzien dat ze zichzelf daarmee diskwalificeren voor professionals.

En je ziet dit niet alleen bij mijn collega's en deze bedrijven, maar ook klanten kunnen het onderscheid tussen 'professioneel' en 'doe-het-zelf' niet goed maken. Bij volwassen vakgebieden is het duidelijk dat je de professional geen advies hoeft te geven over zijn werkwijze of het materiaal dat hij het beste kan gebruiken. Je vertrouwt erop dat de elektricien die je inhuurt je elektrische kabels vakkundig aansluit. Zo gaat dat echter niet bij onvolwassen vakgebieden. Daar krijg je als professional vaak de meest bizarre 'requirements'. Dit maakt het werken in een onvolwassen vakgebied soms wat vermoeiend. 

Gelukkig is er ook goed nieuws. Je hoort namelijk steeds vaker dat 'websites commodities worden'. Hiermee wordt bedoeld dat websites een standaardproduct worden dat iedereen eenvoudig aan kan schaffen. Vaak wordt hiermee bedoeld dat er minder aan verdiend kan worden (iets negatiefs). Ik kan echter niet wachten totdat het zover is. Zou het niet heerlijk zijn als er een duidelijke keuze was tussen zelf doen of uitbesteden, zoals bij werk in je tuin? Besteed je het werk uit? Dan ga je je tuinman natuurlijk niet vertellen wat voor aarde of welk merk gereedschap hij moet gebruiken.

Het gebrek aan onderscheid tussen 'professioneel' en 'doe-het-zelf' wijt ik dus aan het onvolwassen zijn van het vakgebied: het feit dat websites nog geen commodities zijn. Nog teveel mensen denken dat ze een 'website developer' nodig hebben wanneer ze een website willen. Ter vergelijking: mensen weten dat ze naar het tuincentrum kunnen en zelf hun tuin kunnen aanleggen. Hierdoor kloppen ze ook met een hele andere houding en verwachting bij een tuinman aan. Het is tijd dat dit ook zo gaat werken bij 'website development'.

Wil jij doe-het-zelven? Geen probleem! Het internet staat vol met tips en er zijn tools zat. Wix is een goed voorbeeld. Heb je daar geen zin in of heb je een bijzonder of moeilijk project? Neem dan eens [contact](/nl/contact/) op!
