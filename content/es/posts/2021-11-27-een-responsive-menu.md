---
title: Een responsive menu
---

Ik bouw al jaren websites. Sinds 2008, toen de iPhone uitkwam, zijn dit voornamelijk 'responsive' websites. Dit zijn websites die zich aanpassen aan je schermgrootte en dus ook werken op een telefoon. 

Een van de lastigere dingen bij het responsive maken van een website is het menu. Waar je bij de content/tekst de meeste blokken gewoon onder elkaar kunt zetten (plaatje erboven of eronder in plaats van ernaast), komt bij het menu vaak een 'hamburger' kijken: een knopje met drie streepjes dat het menu 'uitklapt'. Ik gebruik altijd media queries (CSS) om mijn websites te laten omschakelen naar de mobiele weergave van het menu. Daarbij definieer ik de breedte waarop dat moet gebeuren vaak in 'rem'. Dat ziet er als volgt uit:

```
@media screen and (max-width: 50rem){
    /* stijlen voor de mobiele weergave */
}
```

Dit betekent dat de website bij ongeveer 50 karakters schermbreedte moet omschakelen naar de mobiele weergave. Dit heeft wel een nadeel. Wanneer iemand meer items aan zijn menu toevoegt kan het zijn dat hij te laat omschakelt... of, in het omgekeerde geval, te vroeg.

Ik liep tegen dit probleem aan omdat ik bezig was met een thema dat door verschillende websites gebruikt wordt. De ene website heeft wel 8 items in het hoofdmenu, terwijl de andere er maar 3 heeft. Dat vraagt om een ander omschakelmoment. Hoewel je kunt kiezen voor de [two viewports to rule them all](/blog/two-viewports-to-rule-them-all/) tacktiek, maar ik maak mijn websites liever ECHT responsive. Vandaar dat ik op zoek ben gegaan naar een slimmere oplossing.

De website zou eigenlijk pas naar het mobiele menu moeten omschakelen als het menu niet meer past (in de breedte). Hij moet dit moment als het ware zelf detecteren. De oplossing is even voor de hand liggend als simpel. Wanneer het menu niet meer past, kun je het automatisch naar twee regels laten springen. Dit kun je relatief makkelijk met javascript detecteren. Ik schreef de code hieronder en zette deze in de footer. Deze code doet deze detectie en voegt een class 'mobilemenu' toe aan de body indien van toepassing. Vervolgens is het slechts een kwestie van de juiste CSS om de hamburger zichtbaar te maken en het menu (tijdelijk) te verbergen. 

```
  function mobilemenu() {
    document.body.classList.remove('mobilemenu');
    if(document.getElementById('menu').offsetHeight>60) {
      document.body.classList.add('mobilemenu');
    }
  }
  window.addEventListener("resize", function() {
    mobilemenu();
  });
  mobilemenu();
```

Het voordeel van een dit script lijkt me duidelijk: het mobiele menu wordt pas zichtbaar wanneer dat ook echt nodig is. Er zijn echter ook wel nadelen. Het controleren van de breedte van het menu tijdens de 'resize' kost rekenkracht (meer dan een CSS oplossing). Daarnaast werkt deze oplossing niet wanneer Javascript uit staat (of is gecrasht). Ik vind deze nadelen niet zwaarder wegen dan de voordelen.

Na vele jaren websites bouwen 'ontdek' ik deze oplossing nu voor het eerst. Gek eigenlijk, want hij is eigenlijk heel voor de hand liggend. Wat vind jij van deze oplossing? Hoe los jij dit probleem op? Ik hoor het graag!