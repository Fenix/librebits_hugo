---
title: Blog - Artículos
url: /es/blog/
---

Acá comparto alguna notas técnicas sobre Internet en general y el desarrollo web en particular en forma de artículos. Apelo a las denominadas 'mejores prácticas'. Si lo que quieres es leer la versión
en inglés de los artículos acá los encontrarás:   [Versión en inglés](/blog/).
