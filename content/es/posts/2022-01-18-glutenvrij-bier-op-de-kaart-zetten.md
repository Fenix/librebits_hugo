---
title: Glutenvrij bier op de kaart zetten
---

Ik heb een zwak voor (land)kaarten en dat komt waarschijnlijk door [mijn vader](https://www.joop.vdschee.nl/), een geograaf. Wanneer er echter in figuurlijke zin iets 'op de kaart' gezet moet worden, dan moet je bij mijn vriend Wouter van Dijk zijn: een marketeer in hart en nieren.

Wouter heeft een glutenallergie, is gek op bier en heeft affiniteit met het verkopen van (alcoholische) dranken. Hij benaderde mij met de opdracht om glutenvrij bier middels een website letterlijk op de kaart te zetten. Samen benaderden we Marco van Vugt, een ontwerper van drukwerk en digitale middelen en onze gemeenschappelijke vriend en oud-huisgenoot. Hij maakte een mooi ontwerp voor een website waarop mensen eenvoudig de dichtstbijzijnde horecagelegenheid kunnen vinden die glutenvrij bier schenkt.

Of wij een goed team zijn? Zeker! Nu nog zorgen dat we met deze website ([glutenvrijbiertje.nl](https://www.glutenvrijbiertje.nl/)) het glutenvrije bier beter op de kaart zetten!