---
title: Het beste CMS voor Hugo
---

Gebruik je Hugo? Dan is er een CMS dat al je verwachtingen overtreft. Het is een one-stop-shop met alles wat jouw website nodig heeft: een CMS, hosting, afhandeling van formulieren, statistieken en meer. Daardoor win je niet alleen tijd en kosten, maar verlaag je ook de complexiteit van je setup.

- zero-config, dus aansluiten en klaar
- nooit meer wachten (deploys van <1 seconde)
- image resizing in Hugo
- formbuilder in Hugo
- failover met dynamisch DNS
- afhandeling van formulieren conform GDPR
- captcha conform GDPR
- server-side statistieken conform GDPR

Bedenk eens wat je normaal allemaal kwijt bent om je formulieren, je statistieken en je captcha's GDPR-proof te maken... en bedenk eens wat een goede image resizing service je kost... en dan heb ik het nog niet gehad over de tijd die je bespaart met [instant deploys](/blog/oh-jamstack-grow-up/). Dit CMS is goedkoper dan de meeste gratis CMS systemen, vanwege de tijdswinst, de lagere complexiteit en omdat je bij gratis systemen vaak betaalt voor extra features en overages op 'build minutes' en hosting.

Je kunt het CMS bekijken op [cms.usecue.com](https://cms.usecue.com) en ja... ik heb het zelf gemaakt. Er maken inmiddels al 30 (high-end) klanten gebruik van dit CMS en dat zijn onder andere grote namen in de reclamewereld en bedrijven met een miljoenenomzet. De kinderziektes zijn er dus inmiddels wel uit.

Enthousiast geworden? Dan is er nog 1 probleempje... het CMS is niet te koop. Ik heb het opgezet voor intern gebruik. Wil je je klanten TOCH heel graag overzetten naar dit CMS? Neem dan [contact](/contact/) op. Wie weet kunnen we wat regelen... 

PS. Wil je dat niet? Probeer dan eens [CloudCannon](https://cloudcannon.com/). Het is iets minder goed, maar ik heb het jaren met veel plezier gebruikt.