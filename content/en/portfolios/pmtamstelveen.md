---
title: PMT Amstelveen
image: /uploads/pmtamstelveen1.png
mobile_image: /uploads/pmtamstelveen2.png
color: '#519151'
portfolio_url: 'https://www.pmtamstelveen.nl/'
google_score: 97%
speed_index: 1.6s
page_weight: 289kb
order: 2
---

I ported this website from Jekyll to building Hugo websites. This webite is hosted in [the perfect CMS for Hugo](https://cms.usecue.com)!
