---
title: Smitwoninginrichting
image: /uploads/smitwoningrichting1.jpg
mobile_image: /uploads/smitwoningrichting2.jpg
color: '#515391'
portfolio_url: 'https://www.smitwoninginrichting.nl/'
google_score: 78%
speed_index: 1.7s
page_weight: 360kb
order: 2
---

I ported this website from Jekyll to Hugo. in this occasion, we respected the originally included css framework. We did not only build this website to accomodate our workflow, but we are hosting it at
our own [perfect CMS for Hugo](https://cms.usecue.com)!
