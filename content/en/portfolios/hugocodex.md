---
title: Hugo Codex
image: /uploads/hugocodex1.png
mobile_image: /uploads/hugocodex2.png
color: '#515151'
portfolio_url: 'https://www.hugocodex.org/'
google_score: 98%
speed_index: 1.5s
page_weight: 264kb
order: 1
---

I changed from building Jekyll websites to building Hugo websites. We wanted a place to store code snippets and best
practices. The website is fully open source. It even holds a very simple implementation of a webshop. We did not only build this website to accomodate our workflow, but we are hosting it at
our own [perfect CMS for Hugo](https://cms.usecue.com)!
