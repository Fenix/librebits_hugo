---
title: NOCK 
image: /uploads/nock1.png
mobile_image: /uploads/nock2.png
color: '#b1901'
portfolio_url: 'https://koudenhoornontdekken.nl/'
google_score: 97%
speed_index: 1.6s
page_weight: 289kb
order: 5
---

I ported this website from Jekyll to Hugo. Used just "vanilla css". This webite is hosted in [the perfect CMS for Hugo](https://cms.usecue.com)!
