---
title: Expertisecentrum Economie en Handel
image: /uploads/ece1.png
mobile_image: /uploads/ece2.png
color: '#ac1e40'
portfolio_url: 'https://www.expertisecentrumeconomie.nl//'
google_score: 92%
speed_index: 1.5s
page_weight: 389kb
order: 3
---

I ported this website from Jekyll to Hugo. This webite is hosted in [the perfect CMS for Hugo](https://cms.usecue.com)!
