---
title: Jamstack means performance, right?
---

"Jamstack has revolutionized the way we think about building for the web by providing a simpler developer experience, better performance, lower cost and greater scalability", acccording to [Jamstack.wtf](https://jamstack.wtf/). 

Most of us already know these claims. However, the [HTTP Almanac](https://almanac.httparchive.org/en/2021/jamstack) has released its yearly analysis of the state of the web, which is a great chance to verify them. But, first some disclaimers on the research. There are (only) 8,198,531 websites in the dataset. Additionally, the data on detected SSGs is based on Wappalyzer technology, which has some limitations. It can’t detect whether the site was built with certain SSGs such as Eleventy. Only Jekyll, Hugo, Gatsby, Nuxt.js and Next.js show up. Although these are expected to be the five largest SSG's around, this is still not very acurate.

We started this article with Jamstack.wtf claiming 'better performance' for Jamstack websites. But do Jamstack websites actually have better performance? Let us look at the median Lighthouse performance score from the HTTP Archive ([source](/uploads/jamstackperformance2021.png)):

|||
|----------|--------------------|
|Next.js|27|
|Nuxt.js&nbsp;&nbsp;&nbsp;|24|
|Gatsby|37|
|Hugo|59|
|Jekyll|69|

Jekyll has the best performce with an average of 69. It is far from [a perfect Google performance score](/blog/how-to-get-a-100-google-lighthouse-score), but it is not bad. Hugo's score is a lot lower. Remember that 50 is the score of the 'average' (median) website. The extremely low scores of Gatsby, Next.js and Nuxt.js are worrysome, especially when you look at the enourmous popularity of those three projects. Is it still valid to say that SSG's have better performance? Not if you look at these statistics. On average they perform worse than non-Jamstack websites.

What, wait, why? I hear you ask. How is that possible? Well, the answer is quite simple. These three SSG's load gigantic amounts of javascript. Let's look at the median size (in kilobytes) of javascript that is loaded by each SSG ([source](/uploads/jamstackjsweight2021.png)):

|||
|----------|--------------------|
|Next.js|746|
|Nuxt.js&nbsp;&nbsp;&nbsp;|713|
|Gatsby|645|
|Hugo|117|
|Jekyll|129|

Hugo loads 117 kilobytes of javascript on average, which is still more than 10 times as much as this website loads. This website loads only 10kb of javascript, uses Hugo and has [a Lighthouse score of 100](/blog/how-to-get-a-100-google-lighthouse-score). Loading 700kb of javascript, however, guarantees a very poor score. I think some Next.js and Nuxt.js fans will claim that their websites load faster AFTER the first page load... but then they already lost 50% of their visitors (especially from places with poor connections).

## Conclusion

Can Jamstack websites have better performance? Yes. Do they on average? No. This is caused by the enourmous amounts of javascript popular SSG's load. The web suffers from [obesity](/blog/website-obesity) and we are about to pass an 'average' (median) size of 2MB for mobile websites ([source](https://httparchive.org/reports/state-of-the-web#bytesTotal)). The Jamstack is not changing any of that.