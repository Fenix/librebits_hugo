---
title: Jamstack is eating the world
---

Netlify ran the [2021 developers survey](https://jamstack.org/survey/2021/) and received over 7,000 responses, more than twice as many as last year. The community seems to have grown, adding not only experienced developers, but also becoming the first stop for those just breaking into the industry.

Jamstack seems to be a rising technology, especially among students. You can review the methodology document for a detailed breakdown of the demographics and accuracy of the survey sample and results.

## Development priorities

What attracted my attention were the following development priorities:

![](/uploads/priorities_2021.png)

Seven thousand developers have put performance on number one. That is nice, because I just created [a CMS for Hugo](https://cms.usecue.com). Hugo is the best performing SSG in the world, building the most complex websites in less than a second. Additionally my hosting solution is able to serve your website in [0.1 seconds](/blog/websites-that-load-instantly), which is [faster than any CDN](/blog/faster-websites-with-a-cdn/) is able to do. 

Uptime is number two, which is also great, as I use [UptimeRobot](https://uptimerobot.com/) to [monitor my uptime](https://uptime.usecue.com/) 24/7 and I have a dynamic failover (a high availability solution) on my servers. 

Also interesting is the rising importance of compliance. I had already seen this coming and warned the community for it. Jamstack websites are notoriously hard to make (GDPR) compliant. While creating my CMS and hosting solution I focussed on privacy friendliness and GDPR compliance, by integrating both analytics and form handling. Becoming GDPR compliant has never been easier.

The last thing that is on the rise is the fear of vendor lock-in. Where other CMS systems require endless configuration, requiring a large investment in their technology, I created a (SSG specific) 'zero-config' CMS. Just connect your website to the CMS and you are ready: no configuration, no learning... nothing! This makes the vendor lock-in almost absent.

## On the road to success

It is great to see all my hunches about the community being confirmed. Speed, reliability and compliance are currently the three most important factors in our industry, which is exactly what I focussed on. The combination of Hugo, Usecue CMS and our custom hosting solution, scores higher on those three points than ANY of our competitors currently do. Even big companies like Vercel (valued at 1 billion dollars) and Netlify cannot get close to our performance. People also want speed of development and are afraid of vendor lock-in. A zero config CMS, like Usecue CMS, is the ultimate solution for that. 

Are you a (Hugo) developer who can not wait to try it all out? I would love to get you on board. [Contact me](/contact/)!