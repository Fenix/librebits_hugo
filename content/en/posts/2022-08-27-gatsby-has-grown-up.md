---
title: Gatsby has grown up
---

In February I created [a post](/blog/oh-jamstack-grow-up) where I told the Jamstack 'to grow up'. I stated that waiting more than a few seconds for a site to deploy is not acceptable, while that is still normal for most non-technical SSG users. Kyle Mathews (CTO of Gatsby) has acknowledged this recenty and solved it in [Gatsby Cloud](https://www.gatsbyjs.com/products/cloud/). 

Kyle says: "Site owners need one second publishes. Whether they’re previewing changes, publishing a typo fix for a news article from a CMS, or there’s real-time pricing or inventory updates on their ecommerce site, it’s critical the change goes live immediately." And he proudly adds: "This experience is what we’ve shipped."

Kyle is using big words, like 'need' and 'critical' and although I agree, I think this is remarkable. Nobody I am aware of (apart from me) has talked about build and deploy times like that before. I am just a dev, but Kyle is a widely known and highly visible person: the CTO of Gatsby.

Another thing that is remarkable is the tech behind Gatsby (the Static Site Generator). Gatsby is based on Javascript and uses React and NodeJS, which are quite slow. Simply rebuilding all pages would take minutes. Hugo can build a similar website in under a second, because Hugo is about [40 to 250 times](https://css-tricks.com/comparing-static-site-generator-build-times/) faster than Gatsby. 

So, how did Kyle manage to get this one second publishing experience? Kyle created a new strategy that does not blindly rebuild all pages, but caches parts that do not change. He calls this strategy [Reactive Site Generation](https://www.gatsbyjs.com/blog/re-introducing-gatsby-a-reactive-site-generator/). I think it totally changes how Gatsby is perceived and how it can and will be used.

It is without doubt an amazing achievement and 'huge' for Gatsby. Gatsby has grown up, or as Kyle says: "Despite having worked on Gatsby for 7 years now, in many ways I feel like we’re just getting started!" I cannot agree more.

Which SSG is next?
