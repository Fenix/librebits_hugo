---
title: Oh Jamstack, grow up!!!
---
Jamstack is showing rapid growth these days. Much has been said about the benefits of the Jamstack (SEO, speed, security). However, having a statically generated site comes with at least one significant drawback: build time. Every update requires your static site generator to recreate all pages. 

A (single) change on a 100+ page website takes somewhere between 2 and 16 minutes [on your average Jamstack platform](https://dev.to/alex_barashkov/measuring-gatsby-projects-build-time-using-paid-plans-of-popular-static-website-hosting-platforms-47pp). Waiting that long before your change goes live is almost unusable! Can you ask your clients to accept that? Even a free Wix or Wordpress website updates instantly. 

Fortunately, it does not have to be that way. A Jamstack website should not only load fast, it should also update instantly... and that is perfectly possible.

> A Jamstack website should not only load fast, it should also update instantly.

Take this website for example. I have created a [screencast](/uploads/cms_update.gif) of updating this 100+ page website through our in-house crafted CMS on our own infrastructure. Note that we do not have thousands of dollars per month for infrastucture (like Vercel or Netlify) and our CMS is bootstrapped. Yet, we can still achieve sub-second deployments. 

How that is possible? Those platforms did not design their infrastructure with the end-users experience in mind. Sure... in a few years they might change their design... but they are simply not there yet. You have to realize that the Jamstack is relatively new and that it is mostly technology driven. Companies still need to figure it out.

Next time you are frantically hitting refresh, while your Jamstack website updates, just send me a message. I am more than happy to get you the benefits of the Jamstack architecture without the drawbacks. 

The alternative is to wait for (the rest of) the Jamstack to grow up.
