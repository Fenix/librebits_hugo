---
title: How to pitch a Jamstack website to your clients
---

CloudCannon [made a list of questions](https://cloudcannon.com/blog/how-to-pitch-a-jamstack-website-to-clients/) a (stereotypical) client might ask about the Jamstack. I listed their questions and answers below. I wrote a comment on each question/answer.

> ‘Static sites’? Isn’t that the way the web used to work? It sounds like it’s not exactly, well... dynamic.

CloudCannon: The difference between a static site and a dynamic site is a little technical, but worth understanding. With a dynamic site, the server gets the content from a database and builds the page for every single visitor. Static sites, by contrast, are pre-built, so when a visitor loads a page, the server simply returns a file. This is why static websites appear near-instantly, while a typical dynamic site can take seconds to appear.

Joost/me: Do not call them 'static sites'. Always call them 'Jamstack websites'. You can call them 'secure', you can call them 'fast' and you can call them 'modern', but you should not call them 'static'. Let's face it... a dynamic website with a CDN is just as static as a Jamstack website.

> It sounds complicated. Do you have to be a developer to update the content?

CloudCannon: Not at all. There’s a growing range of modern and user-friendly Jamstack content management systems built specifically for content editors and non-technical folks. (And if content marketing teams are uploading and editing their own work, it frees me up to develop more efficiently and keep improving their sites!)  
In fact, with a CMS like CloudCannon, editors can even see their on-page changes as they’re editing. You’ll have no more guesswork about how the preview page will look! And the editing interface will be familiar to anyone who’s used even a basic text editor before, with intuitive additions like page components that even let you make new pages, completely independently.

Joost/me: Do not talk about technical details. Just tell them they will get 'a website with a CMS' and that it is better than WordPress. Offer them some free training if they are still unsure. To the client it should still be the exact same product: a website with a CMS.  
Never tell your customer anything about visual editing (on-page changes). They will immediately compare your software to Wix. It will make you look foolish and will make your solution look very limited. The same goes for previews. They will know that it leads to people indenting stuff with spaces and messing up their content to a point where even the search engines no longer find it attractive. We have been there... 15 years ago.  
Avoid bragging about the ability for them to create new pages on their own. Any CMS should facilitate that. Do not make a fool out of yourself.  
Do not tell them that they can create their own layouts. If it is a remotely important website, they should not be able to do that, or you will end up with 30,000 pixel long monstrosities as pages. Tell them changing the layout in a Jamstack website is very easy for you as a developer. Tell them they only need to worry about the content and that you are offering a separation between design and content. Change their perspective.

> What about SEO and analytics? Can I track everything I currently do in Google Analytics?

CloudCannon: Absolutely! Google Analytics works with static sites just as easily as it does with a legacy CMS like WordPress. (And static sites typically load faster than dynamic sites which leads to better ranking on search engines.) High Lighthouse scores — on Performance, Accessibility, Best Practice and SEO — greatly impact search rankings, and static sites typically have higher Lighthouse scores. All of this means a better user experience, optimized capacity for your customers, and ultimately increased conversions for you.  
It’s worth pointing out that all of your current marketing tools can continue to work, as well — Crazy Egg, Clarity, Matomo, Hotjar and more. Compare these and more Jamstack-compatible analytics tools here.

Joost/me: Yes, you can still use Google Analytics, but it has recently been declared illegal (in the EU). Therefore you should use server logs.  
SEO does suffer, as most Jamstack CMS systems do not have contextual SEO advice (like the Yoast plugin in WordPres). However, configuring a WordPress website correctly (from a SEO perspective) is much more complicated than configuring a Jamstack website. So you will most likely play even here.  
The (SEO) speed advantage is a myth. A WordPress website (with a CDN) can be exactly as fast as a Jamstack website. In fact... it turned out that most modern Jamstack websites are actually slower (because they use an aweful lot of Javascript).

> But will it cost more money?

CloudCannon: It depends how much you want to pay me! Seriously, setting up a Jamstack site is no more expensive than setting up a new site with a legacy CMS. With the developer efficiencies that Jamstack CMSs like CloudCannon bring, we can spend more time on the frontend polish that you and your users will actually see, rather than behind-the-scenes boilerplate code.

Joost/me: The rule of thumb here is that it depends on the complexity of the website. While a complex Jamstack website is more expensive than its WordPress/dynamic counterpart, a simple Jamstack website is less expensive.

> How secure would this be?

CloudCannon: Static sites are inherently more secure, because they’re not running code on every single page load. To put a finer point on it, there’s simply less surface area for a hacker to exploit — a user’s browser is doing the work of rendering the page, not a server halfway across the country (or the world).

Joost/me: Be open about this. Using different services makes the attacking surface bigger, while the lack of dynamic code execution makes the surface smaller.

> How much choice do I have, in terms of the look and feel of the site?

CloudCannon: Static sites have the same fully flexible design and functionality options as any dynamic website. In fact, given that developers can be more efficient with our time and the fact that we don’t need to worry about incompatible WordPress plugins affecting our sites, we have more scope than ever before.  
When it comes to how the site is built, we can work together to choose the most suitable static site generator from those offered by CloudCannon. Sites with thousands of pages and multiple images would benefit from using Hugo, for example, while Jekyll remains a popular choice for corporate websites, blogs and product documentation. Newer static site generators like Eleventy, Next.js, and SvelteKit are also becoming increasingly popular for websites and web applications alike, due to their lightweight and efficient structure.

Joost/me: The correct answer is: 'none' or 'infinite'. Tell them you avoid page builders and that you use a strict separation between design and content. Tell them you can easily change their layout, but that roles are separated. If they do not like your answer, tell them to use a less professional solution, like Wix, where design and content are mixed up.

> What about search? Or contact forms? Oh, and e-commerce? Can we set up an online shop?

CloudCannon: Of course! The Jamstack ecosystem includes a wide range of customizable services, plugins and tools, from analytics to community tools, e-commerce services, search functions and more. If it’s possible anywhere on the web, it’s possible with CloudCannon.

Joost/me: Be honest here. You get these things for free in a WordPress website, while they are paid services in the Jamstack ecosystem. Most people do not care about that. However, the legal risk of adding extra services might be a problem. Be open about that and be ready to provide a solution.

> What if we want to change the site in the future? Or move to a new CMS?

CloudCannon: All of your content is managed by Git, and lives in your repository. It remains yours, and isn’t locked away by CloudCannon or another third party. Actually, your data is more portable in Git than it would be in large database backup files.  
Content changes are easy, too. CloudCannon allows content editors like you to see all of their on-page changes instantly, using the most intuitive and configurable interface out there. We can choose from a range of editing controls to enable, according to each site’s use case, and can even give each content team a personalized interface.  
It’s not just about swapping in text and images, though. We can create complex layouts with prebuilt nestable components, and share full component libraries with editors. Even non-technical editors are empowered to build new pages using components. Using an intuitive drag-and-drop interface you can shuffle, copy, and reorder them on the page; and update the components’ content each time, if you’d like.

Joost/me: Do not talk about content changes, you will look foolish. Any website should be editable.  
Do not talk about Git. It is technical and it does not matter to your client. Talk about advantages and qualities they care about. Tell them that you can revert their website back to any point in time.  
Offer them access to the files, instead of debating openness and reusability. Tell them the Jamstack community is thriving and that there are many Jamstack developers who can take over your work.  
Again, do not talk about them changing the layout of the website. Any self-respecting manager knows that their staff should not be able to do so and you should always avoid the comparison to something like Wix.

> But if we rely on open-source tools and software, who will support them? What about maintenance costs?

CloudCannon: These are legitimate concerns for many businesses, and also the reason why so many choose more expensive software options. But here’s why you don’t need to worry. Thousands of developers contribute their time to open-source software projects (like SSGs), many of which have received ongoing financial support from companies and independent developers who believe in the respective projects. And CloudCannon’s CMS is, of course, a service. Their support team helps agency developers like me with any issues relating to the site behind the scenes. Agency devs help set up clients like you with the design and constrained page elements you need. And you’ll have access to easily change the site’s content, layout, and everything your users will see.  
Besides, with a CMS like CloudCannon, there’s no ongoing maintenance. Where a normal WordPress site might rely on juggling plugins and additional services — each with its own insecurities, risks and compatibility issues — CloudCannon is a very lean stack.  
This means sites are more stable no matter how often the content is updated, and clients don’t need to spend their money on basic maintenance requests. Developers like me, for our part, can spend more of our time doing what we do best — making the best sites, more efficiently.

Joost/me: Do not tell them about open-source. Clients do not care about open-source. WordPress is open-source as well and nobody cares. Do tell them about the possibility to use multiple CMS systems. Offer them to install both CloudCannon, Netlify CMS and Forestry.  
Stay away from talking about funding. We all know where funding leads to: instable businesses with volatile pricing. Talk about the different options they have within the Jamstack and the size of the community.  
Do not talk about dependencies on service and support teams. Everybody knows that too many layers lead to bad support. Tell them you can fix most things yourself. Keep it positive.  
CloudCannon is everything but a lean stack. It supports many SSG's, has a ton of features and gets regular updates, which occasionally lead to bugs. Do not tell your client any of that. This is your risk and the risk is perfectly acceptable.

> This... actually sounds great? When can we start?

CloudCannon: As soon as you want! In fact, I can sign up for a 14-day trial with CloudCannon right now. Or, you can browse ready-made templates here. It’s even possible for me to deploy a sample portfolio site, or a product / SaaS business site, in a single click.

Joost/me: Not a real concern.

> So let’s say we do it. We save money, our team works more efficiently — so far, so good. But what about our customers? No one likes change.

CloudCannon: A faster and more responsive site is always a better user experience. If you run a transaction-driven site, then giving your users a fast experience is a key way to differentiate from your competition. But if you think you have to convince anyone on this point, just look at the stats: Website performance has a huge influence on how customers perceive a brand — a one-second delay in website load time reduces customer satisfaction by 16%. Every 100ms delay in site load time can reduce conversion rates by 7%. When mobile page load times balloon from 1s to 3s, bounce rates increase by 32%. (If your site takes 5s to load, you’d be looking at a 90% bounce rate!) Google’s search algorithm privileges sites that load faster than average. (CloudCannon’s median page load time is 6x faster than the web’s average.)

Joost/me: Do not sell clients a conversion to a Jamstack website. Sell them a new and better website. Improve their design along the way. Maybe just sell the whole thing as a design upgrade. People do not like change, but they like new things.

> What about downtime? Will my website stay online?

CloudCannon: Yes. With a content delivery network (CDN) of over 250 data centers around the world, CloudCannon has maintained 99.999% uptime in the last 48 months to date.

Joost/me: Be honest. You will have no control over this. CloudCannon or Netlify will out-source your website to Cloudflare or AWS. Your customer will experience any downtime they have, which they probably understand and accept.

> What’s the catch? What’s in it for you?

CloudCannon: There’s really no catch. I’m still designing and developing; you’ll still receive a beautiful and future-proofed website. But in this case, with CloudCannon it’s a site that you’ll actually enjoy editing.  
You’re closer to your content than anyone else could be. It’s your content editors who should see the final product on the page in those last few seconds before it goes live — they’re the ones who will catch any last-minute corrections!

Joost/me: No catch? Sure there is. Tell them you will be a happy developer. Tell them you can create more in less time. Ask for this favor. Why not?  
Do not lie about 'time to live'. Dynamic websites update instantly, while Jamstack website generally take seconds or minutes to update. Instant previews in CloudCannon can give a pretty good impression of what it will look like... but it could still be broken online (which you are unlikely to check after seeing the preview). Just tell them that it takes some time to update a website. Tell them that you will switch them over to Hugo if it gets too bad. 

> Ok, but who else actually uses Jamstack sites? I don’t want to do it if bigger companies or successful brands haven’t already used this approach.

CloudCannon: Netflix uses Jamstack (via CloudCannon) to create an extensive network of international sites to maintain and manage relationships with its customers and partners. Twitch uses Jamstack (via CloudCannon) to deliver sites to their 30 million daily active users. CloudCannon’s marketing site itself uses Jamstack, created of course with their own CMS. [...]

Joost/me: Name dropping never hurts... so yeah.. why not.
