---
title: The fastest website in the world
---
"This is the fastest website in the world. Don’t even try, you will not be able to find anything as fast as this website!"

This is what Mfon Abel Ekene wrote [on this website](https://fastestwebsite.net/). He continues: "I am tired of people, clients, and developers being too much obsessed with speed. The only way to have a super-fast website is to have a website with almost nothing!"

I do not agree to that... and yes, I am [obsessed with speed](/blog/websites-that-load-instantly/). "Don't even try" sounds like a challenge to me. Where Robin J.E. Scott claims to have *the fastest website in the world* with a [top ranking monstrosity](https://robinjescott.com/fastest-website-world/) of 1Mb, Mfon has created a much [lighter page](https://fastestwebsite.net/) at 172kb. However, that is still 100kb larger than this website. 

Why is his page still so large? Let's analyze the network tab in our developer tools. It will show us the following:

- 5kb is wasted on a favicon.ico file, which is overruled by a PNG
- 7kb of javascript is loaded for a menu that does not exist
- 19kb(!) of CSS is loaded, while there is barely any styling visible
- 18kb of unused javascript is loaded for displaying emoji
- 87kb of unused CSS is loaded for non-existent Gutenberg blocks
- 33kb of HTML is loaded, while this could easily be 10kb

You could trow away 95% of the code without changing anything (visually). Your new page would consist of a few lines of CSS, a favicon and some HTML. Do you want proof? I recreated Mfon his website... [check it out](/fastestwebsite/index.html)! It weighs 9kb, almost 20(!) times less than its original. It [loads in 71ms](https://tools.pingdom.com) (from Frankfurt), which makes it up to two times faster than *the fastest website in the world*.

Although I agree with Mfon that size or speed should not be the goal itself... and although Robin J.E. Scott has proved (once again) that size and ranking are not related... I DO believe that size and speed are important. They do not only improve the user experience, but they are also indicators of build quality. [Code is poetry](/blog/code-is-poetry/). This is exactly the reason why I publish the [Google score](/blog/google-lighthouse-score/) in [my portfolio](/portfolio/the-baby-signpost/).
