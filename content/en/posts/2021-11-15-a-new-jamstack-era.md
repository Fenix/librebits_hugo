---
title: A new Jamstack era
---

I started building Jamstack websites in 2015. In the past 7 years I never experienced any downtime. I have built 250 websites with Jekyll on CloudCannon. The speed of the websites and the CMS was quite good, especially when you compare it to WordPress on shared hosting. I also had relatively little maintenance work on my websites and not a single one got hacked and/or got infected with malware.

"Don't change a winning team", is what they say. But there were also some problems. I also found out that everything could be faster: both hosting and updating the website could be 'instantly'. Additionally, the whole solution was not very privacy friendly. But the straw that broke the camel's back was the fact that my supplier raised its prices significantly.

## Hugo 

I was building al my websites with Jekyll. Jekyll was a safe and easy choice. In 2015 Jekyll was the most popular SSG with the largest community. I became so good at building Jekyll website that I showed at JekyllConf 2019 how to build a Jekyll website with a CMS and form builder in under 20 minutes. Nevertheless I have chosen to switch to Hugo. Hugo is harder to learn, but offers a big advantage: ultimate speed. When you make a change to your Jekyll website, you have to wait up to 5 minutes for it to be online. With Hugo the waiting is a thing of the past. Almost every change results in instant regeneration of your website. Additionally Hugo is able to scale and compress images, which is very important in optimizing websites.

## My own CMS

Since 2015 I was using CloudCannon as my CMS. Initially CloudCannon only supported Jekyll. CloudCannon is fast, easy to use and it has many great features. However, it recently became much more expensive. The only reason for this, I can think of, is that CloudCannon failed to attract sufficient investments and/or grow quickly enough. While this was a bummer, it was also the ultimate chance to build my own ideal CMS: technologically advanced yet easy to use. I succeeded and called it Usecue CMS. You can find my CMS on https://cms.usecue.com. I connected the first 20 websites to this CMS and so far the response has been great!

## My own servers

I used CloudCannon hosting for a lot of clients. CloudCannon hosts on Cloudflare. There are [many reasons](/blog/why-you-should-avoid-cloudflares-cdn) not to host your website on Cloudflare of which [speed](/blog/faster-websites-with-a-cdn/) is an important one. Now that I have my websites hosted on my own server, they have become up to 10 times as fast. This is caused by better hardware, but most of all by a smarter caching strategy. The time it takes to push a website to production is also a few milliseconds. Combined with the instant builds of Hugo, this results in sub-second updates of your website.

## My own form handling

Because of ease of use, I used form handling from third parties, like the American Formbucket or the form handling offered by CloudCannon. Because of the GDPR everybody had to check a checkbox upon submit to provide explicit consent for using this third party. That was not very professional. Additionally we suffered more than once from downtime, which on one occassion even lead to loss of income for one of our clients. That is why I have build my own form handling software. I integrated it into the CMS for ease of use.

## Statistics through log analysis

Google Analytics is a nice tool, but it becomes increasingly difficult to use it [conform the GDPR guidelines](). That is the reason I chose for log analysis, just like Netlify, CloudCannon and other big companies. The disadvantage is that you get less information about your visitors. The advantage is that the numbers are far more reliable. I integrated these statistics also into the CMS.

## Concluding

I have been able to improve my service/product on many points. This was partly fueled by the price raise of my supplier. Obviously it was not cheap to build everything myself, but it enables me to provide a higher service level and focus on a higher market segment. I am also the first who can make a website load in 0.1 second and update it in less than a second. 

In short: I build privacy friendly websites that can do whatever you need them to do and that are quicker than any website you have ever seen before. Are you interested? Contact me!