---
title: The website carbon calculator
---

Michelle Barker featured the [website carbon calculator](https://www.websitecarbon.com) [on CSS-Tricks](https://css-tricks.com/reduce-your-websites-environmental-impact-with-a-carbon-budget/) today. She wrote a nice piece about the application of such a tool. Her suggestion to add it to the [Google Lighthouse Score](/blog/google-lighthouse-score/) is a good one.

Google [claims](https://www.google.com/about/datacenters/efficiency/) to have a PUE of 1.1. Adding the 'carbon score' to Lighthouse is a great way to further green wash their data hungry business model. However, I agree with Michelle that adding a 'carbon score' could have a positive effect and is actually a good idea.

Because I did like the tool, I also got interested in the company behind it. This appears to be [Whole Grain Digital](https://www.wholegraindigital.com/), a web development agency from London. I think they have done a terrific job on the design! The quirky feel of their tool gives me a very positive vibe. The image they showed me when I entered my websites address made me genuinly happy. Here it is:

![Website Carbon Calculator](/uploads/greenwebsite.png)

But something hit me. My website generated 0.02 grams of CO2 per visit, while when I entered the tool itsself I got a score that was 21(!) times as polluting! Why? What went wrong there? The design is as simple as mine.

I digged into their source code.

1. they use WordPres for their website, which is a remarkable choice for a [green website](/blog/a-green-website)
1. they load [a huge 102kb javascript file](https://www.websitecarbon.com/wp-content/themes/carbon-calculator-theme/assets/scripts/scripts.js?ver=1621438051) that loads some custom script and the Unipointer, Flickity and Unidragger library
1. these javascript libraries are quite heavy, but are not used in their website (as far as I can tell)
1. they load 144kb of (compressed) CSS, which is 10 times the size of a 'normal' CSS file
1. for their statistics they use a service that is called [Cabin](https://withcabin.com) (low footprint analytics), which is created by [Normally Ltd.](https://normally.com/) who are autoplaying 12MB of video on their homepage
1. this makes the Normally homepage not 21 but [337 times](/uploads/normally.png) more polluting

So, to get things straight: this company, that is doing a 'not so good job' at building a green website, is telling me I should hire them for my next green website? While they are using a tool from a company that claims to care about footprints, but has a 12MB homepage? It just feels wrong.

The tool is great. I applaud the idea, I applaud the design and I applaud the marketing effort, but from a technical point of view the tool is disappointing. And isn't technical excellence what this tool is actually about?