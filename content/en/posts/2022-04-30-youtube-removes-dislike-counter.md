---
title: Youtube removes dislike counter
---

In november I wrote [a negative comment](/blog/google-makes-me-sick/) under a [Youtube video](https://www.youtube.com/watch?v=Df2U9-R-OJs) from the Chrome team. I did not agree with its content. I felt like we were being manipulated by the way things were presented.

I was happy to find that I was not alone. A lot of people found the video bad and clicked the 'thumbs down' button. The dislike counter was just as high as the ‘thumbs up' counter. When I came back to that video today, to see what the current state of affairs was I was unpleasantly suprised. Youtube had decided to remove the dislike count. I was flagged by a comment below this video that read ‘Return dislikes!'. I immediately looked at a few other video's. Their dislike counter had also been removed! I turned out that Google had removed all dislike counters from Youtube on December 13th, 2021. 

Is this a bad thing? Well... negativity is not a good thing. However, removing the dislike counter has a weird effect. This video was clearly disliked in the past, even though the Chrome team did a thorough job of removing negative comments. With the removed dislike counter you could no longer tell. Therefore I did some research. I created the table below of Chrome Dev Summit video's with their amount of views and likes. I calculated the percentage of people that gave the video a thumbs up. This was 1% in 2020 and 3% in 2019. This highly debated video had a like percentage of 0.03%! The comma is in the right place here folks. I have not found ANY video on Youtube with such a low ‘like rate'. A random cat video scores 1.4% and even awful shootings in the US score higher than 1%.

||views|likes|percentage|
|----------|--------|-----|-------|
|[Chrome Dev Summit 2021](https://www.youtube.com/watch?v=Df2U9-R-OJs)&nbsp;&nbsp;&nbsp;| 1.2M | 417 | 0.03% |
|[Chrome Dev Summit 2020](https://www.youtube.com/watch?v=14pb8t1lHws) | 55K | 560 | 1.02% |
|[Chrome Dev Summit 2019](https://www.youtube.com/watch?v=-oyeaIirVC0) | 0.25M&nbsp;&nbsp;&nbsp;| 7.5K&nbsp;&nbsp;&nbsp;| 3.00% |
|[Random cat video](https://www.youtube.com/watch?v=epA_8nN1y4I) | 0.9M | 13K | 1.44% |

So... yes... removing the dislike counter (in combination with Google's cencorship) is dangerous. The subject in this video is 'web technology' (worth billions). What if this video contained misinformation about a war? Completely removing the public opinion makes it easier to manipulate people, promotes lies and it creates 'content bubbles', which fuel extremism.

What now? Well... for now we have to look at the ‘like rate'. I feel that content with a like rate that is far below 0.5% serves nobody and should be ignored (or removed).