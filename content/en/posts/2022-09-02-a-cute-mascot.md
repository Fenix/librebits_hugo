---
title: A cute mascot
---

I added a mascot to my posts. It is a small (living) bot, made of simple HTML characters. It lives in the footer of each blog post and is part of my (new) signature. 

The bot will react (differently) to moving your mouse over the bot, pressing the left mouse button (outside of the bot) and moving the mouse around the screen. When the bot is looking at you it will wink every now and then. When the bot is looking away it only blinks in a semi-random interval.

I am a big fan of [Japanese graphical design](https://en.99designs.nl/blog/design-history-movements/japanese-design/), including their [famous patterns](https://duckduckgo.com/?t=ffab&q=famous+japanese+pattern&ia=web). The overall design of my website is (not accidentally) a reference to this style: the website is minimalistic, geometrical and contains natural elements (the leafs). Additionally, my splash screen on the homepage contains a Japanese looking pattern, made out of 'windows' (squares). 

Companies having mascots is [quite normal in Japan](https://tokyotreat.com/blog/japans-mascot-obsession-cute-kawaii-crazy-japan), so I thought it would be fun to add one as well. The little bot/character is very [kawaii](https://www.merriam-webster.com/words-at-play/kawaii-cute-japan-origin-meaning), which is an important esthetic quality in Japanese culture. 

The design of my website is also [a tribute to the web](/blog/a-tribute-to-the-web) and the bot is (not accidentally) a form of ASCII art. The ASCII art (obviously) refers to the early days of computers/the web. This way the two storylines in the design ('Japanese esthetics' and 'the early web') come together once more.