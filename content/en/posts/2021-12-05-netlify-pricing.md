---
title: Netlify pricing
---

I wrote about [Netlify pricing](/blog/how-netlify-became-irrelevant/) when Netlify introduced analytics for 9 dollars a month per website, back in 2019. I compared it to the CloudCannon pricing and called Netlify 'irrelevant'. 

A lot has changed since then. Netlify has added a 'unlimited' plan to their offerings at 99 dollars a month. This was a little higher than the 'unlimited' plan from CloudCannon at 75 dollars a month (both plan included unlimited websites and analytics). However, CloudCannon has removed it's 75 dollar unlimited plan and added a 250 dollar a month 'Business' plan, which is limited to 20 custom domains.

Netlify is still quite pricey when it comes to single websites, but their 'unlimited' plan became a very interesting option. Calling Netlify 'irrelevant' today, would be weird. I would even say that the 'unlimited' offering of Netlify is very relevant. They support any SSG and include 600GB of traffic. Sure, prices start at 1200 dollars a year and the builds are quite slow, but it is a very competitive offer, as you can easily host 30 websites on their 'unlimited' plan without any overages. This comes down to 3 dollar a month per website. That is very reasonable and much cheaper than the 12 dollars a month per website CloudCannon is (nowadays) charging.

Although Netlify became relevant again, from a financial point of view at least, they are not completely out of the woods yet. Netlify is hosting on Amazon Web Services and forms and statistics are stored on the Netlify servers. This makes it hard for European companies to safely use their services (from a legal point of view), as [they are implicitly required to audit GDRP compliancy](https://www.taylorwessing.com/en/insights-and-events/insights/2020/08/compliant-or-non-compliant---gdpr-audits-as-a-self-control-tool). A simple sign off on some legal documents might be all you are currently doing, but that could very well be unsufficient. Failing to be compliant can cost you A LOT of money.

So the question is: What is the real price you pay for hosting at Netlify?