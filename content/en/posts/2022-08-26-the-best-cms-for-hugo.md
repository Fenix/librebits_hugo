---
title: The best CMS for Hugo
---

Are you using Hugo (the static site generator)? Then there is a CMS that exceeds all your expectations. It's a one-stop shop with everything your website needs: a CMS, hosting, form handling, statistics and more. This not only saves you time and costs, but also reduces the complexity of your setup.

- zero-config, so connect and go
- never wait again (<1 second deploys)
- image resizing in Hugo
- formbuilder in Hugo
- failover with dynamic DNS
- GDPR compliant form handling
- GDPR compliant captcha
- GDPR compliant server-side statistics

Think about what you normally spend to make your forms, your statistics and your captchas GDPR-proof... and think about what a good image resizing service costs you... not to mention the time you save with instant deploys. This CMS is cheaper than most free CMS systems, because of the time savings, the lower complexity and because with free systems you often pay for extra features and overages on 'build minutes' and hosting.

You can view the CMS on [cms.usecue.com](https://cms.usecue.com) and yes... I made it myself. Technically it is a hosting setup in combination with a CMS. There are already 30 (high-end) customers using this CMS and those include big names in the advertising world and companies with a turnover of millions. So all bugs have been removed.

Enthusiastic? Then there is one problem... the CMS is not for sale. I use it for internal (client) use only. Would you still like to transfer your customers to this CMS? Please contact us. Who knows, maybe we can arrange something...

PS. Don't you want that? Then try CloudCannon. I've used it successfully for years (and still do).