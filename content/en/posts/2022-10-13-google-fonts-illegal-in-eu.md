---
title: Google Fonts illegal in EU
---

The [instructions](https://developers.google.com/fonts/docs/css2) for using Google Fonts tell you to do something that is clearly illegal in the EU.

This [has been confirmed](https://rewis.io/urteile/urteil/lhm-20-01-2022-3-o-1749320/) more than six months ago by the German court. In the aforementioned case the website owner had to pay 100 euro to the complainer and faced a penalty payment of up to 250k euro if failing to comply. It is remarkable that Google does not warn developers for this infringement, as it has been illegal for years (and Google should know that). This mentioned law suit did not change the law.

Not Google, but Wordpress warned me today for the Google Fonts infringement. Wordpress added a check to the Wordpress dashboard to prevent its users from breaking the law (pointing to this law suit). Thank you Wordpress! This made one thing very clear to me: As a developer you can not trust Google. The people at Google clearly do not care if they put you at risk. 

> As a developer you can not trust Google.

You may say that the risk is only 100 euro, but then you are wrong. The penalties from a GDPR violation (which this is) are much higher. Anyone can file a GDPR violation complaint, based on this violation, which may result into a penalty of up to 400k euro. Additionally, a non-profit could file a class action law suit, resulting in not only much larger financial damages, but also a lot of negative attention. I think the website owner in the example was very fortunate.

Note that there is a legal way to use Google Fonts, as they are not really 'Google Fonts'. Google just collected and rebranded [open source fonts](https://developers.google.com/fonts/faq) and provided an easy (but illegal) way to embed them in your website. The proper way to embed your font is to use [Google Webfonts Helper](https://google-webfonts-helper.herokuapp.com/fonts), a non-Google initiative, where you can download them and host them yourself.

Google... shame on you. You are misleading developers. Paying developer advocates is useless when you put developers at risk like this. Actions (generally) speak louder than words.