---
title: Embedding Youtube correctly
---

Paul Irish did a nice thing. He created a [lighter Youtube embed](https://github.com/paulirish/lite-youtube-embed). His solution does no longer slow your website down, like normal Youtube embeds do. There is only one downside: it loads content from Youtube without consent, while Youtube (Google) is a company that is known for tracking and profiling. This is not very privacy friendly.

The solution is to host the poster yourself and link it to Youtube. When you load the [Hugo Lightbox](https://hugocodex.org/add-ons/lightbox/) the link will automatically get a class 'lightbox-youtube', which then can be styled. This looks like this:

```
[![](/uploads/youtube/Kh1c_N2VZPc.jpg)](https://www.youtube.com/watch?v=Kh1c_N2VZPc)
```

```
.lightbox-youtube {
    position: relative; display: block;
}
.lightbox-youtube::before, .lightbox-youtube::after {
    content: "";
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
} 
.lightbox-youtube::before {
    background: url(/img/youtube_button.svg) center center / auto 4rem no-repeat;
}
.lightbox-youtube:hover::before {
    background: url(/img/youtube_button_hover.svg) center center / auto 4rem no-repeat;
}
.lightbox-youtube::after {
    background: url(/img/youtube.svg) calc(100% - 1rem) calc(100% - 1rem) / auto 2rem no-repeat;
}
.lightbox-youtube img {
    display: block;
}
```

The end result is an instantly loading Youtube 'embed', that opens in lightbox. That means your user will view the video in a non-distractive way, without leaving your website. For a demo, look at [the Viveon website](https://www.viveon.nl/producten/webinars-gehechtheid-in-de-zorg/) or this [Codepen](https://codepen.io/joosts/pen/gOGYNRB).

## Why this is better

This solution is better, because there is no initial cross domain request. This solution also 'warns' your user that he/she will be loading content from Youtube. This is done by showing the Youtube logo over the video. Just to be extra clear: nothing is loaded from Youtube until your visitory actually clicks on the play button. Clicking a play button on a Youtube marked video could be interpretted as 'consent'. This is supported by the fact that links to external websites are also allowed by GDPR. In short: this solution is more privacy friendly.

Apart from being more privacy friendly, loading the poster image from your own server is also quicker: a win-win situation.

Note that my website does not use a CDN, as it is Dutch-only. Therefore it may load a little slow from your geographical location. It loads in under 0.2 seconds in the Netherlands, which you can verify by testing it with [tools.pingdom.com](https://tools.pingdom.com) from Frankfurt (Germany).