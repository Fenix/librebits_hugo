---
title: Faster websites with a CDN?
---

Good websites load instantly. Almost everybody who wants a faster loading website uses a CDN (Content Delivery Network). This is a network of servers, distrubuted around the world, where every websites is available on every server.

## What does a CDN do?

A CDN does two things: it caches your files/requests (make them static and keep them in memory) and it serves them from a server near your geographical location. This should normally save time.

## Static websites

However, for static websites this is slightly different, as the responses to the requests are already static and are already served from memory (when using a properly configured webserver). This means you can only loose time on this front. 

## Local target groups

Because a CDN is a distributed network with nodes all over the world, your website is hosted on a webserver that is relatively close to you, at least that is the theory. In reality websites often serve a very local target group (especially in Europe where they are also in a local language). In that case you want a 'hyperlocal' server: a server that is very close to your target group and not 'somewhere on your continent'.

## How much does a CDN delay?

The delay that is caused by using a CDN can be quite significant. For my customers I used to host Dutch websites on the [Cloudflare CDN](/blog/why-you-should-avoid-cloudflares-cdn/), which resulted into loading times of up to 0.8 seconds, whereas these websites load in 0.1 seconds without a CDN. When accessed from San Francisco (the other side of the world), those same websites load in 1.2 seconds without a CDN. That is not fast, but it is acceptable. So, without a CDN your website loads 8 times faster when accessed locally, while they are not that much slower from the other side of the world.

## What? How is that possible?

Why would anyone use a CDN for a static (Jamstack) website? Aren't CDN’s super popular and the foundation of the Jamstack community? Well... to be honest I do not get that either. With most CDNs you get zero control over the cache. The chance that your not so popular website is still available in the CDNs cache is close to zero, which is caused by their caching strategy. Almost all CDNs use pull caching which favors high traffic websites. Therefore the visitor of your website will almost always get ‘cache miss’ upon the ‘first’ request (which is almost every request in reality). A VM, however, can have enough memory to always serve all requests from cache (just like a push CDN would). This explains the huge speed difference. 

Additionally the CDN nodes (servers) are distibuted around the world, but you have no influence on where they are located exactly. This means they could very well not be on a premium spot or on a spot that is relatively far from your target group.

In reality a well-situated (what I call 'hyperlocal') VM of just 10 euro's a month will offer much better performance than the avarage CDN: strange... but true.

## Blazing fast websites

I build websites with very light pages, because I use no frameworks. That allows me to get super low loading times and [a perfect Google Lighthouse score](/blog/how-to-get-a-100-google-lighthouse-score/). Most of my websites load [faster than you can slice a potato with a potato gun](/blog/websites-that-load-instantly/). 

After reading this, it may not surprise you that I often choose to NOT use a CDN. Although they can make international or high-traffic websites load faster, pull CDNs are almost always a bad idea for websites in a local language or websites with a local target group. This is even more true for Jamstack websites. 

If you want a blazing fast website, just [contact me](/contact/). I would love to help.
