---
title: Thanks to Kev Quirk
---

I got an email from [Kev Quirk](https://kevq.uk). He pointed me at my missing RSS feed, which he apprently does [frequently](https://kevq.uk/please-add-rss-support-to-your-site/). I added it immediately. We exchanged a few emails and I got fascinated by his blog.

Thanks to Kev Quirk, I:

- now have an [RRS feed](https://www.usecue.com/index.xml) on my website
- am now part of the [512kb club](https://512kb.club/)
- know that 300 euro a year is [reasonable](https://kevq.uk/goodbye-wordpress-switched-to-jekyll/) for WP hosting
- got to know some great [blogs](https://kevq.uk/blogroll/) and [podcasts](https://kevq.uk/blogroll/)
- got inspired to write a 'things I use daily' list (see below)
- am curious to learn more about [Fosstadon](https://fosstodon.org/@kev)
- had fun with his [Weird Wide Web Ring](https://weirdwidewebring.net/)
- got mentioned on his [blogroll](https://kevq.uk/blogroll/)

Things I use daily:

- Linux
- Visual Studio Code
- Firefox
- Hugo
- Inkscape
- Gimp
- disk encryption
- wired peripherals, like a conference microphone and a mechanical keyboard

So... Thank you Kev!