---
title: The story of Delete Agency
---

As far as I know there is only one other business that is also good at getting a perfect Lighthouse Score: [Delete Agency](https://www.deleteagency.com/). At the beginning of 2018 they challenged themselves to get a 100% score on their own website, but without changing how the site looked or functioned... and they succeeded!

## Optimizing everything

They optimized their website in every way possible. They created the following checklist:

- use small Javascript files
- tree shaking
- code splitting
- use ES2015 modules
- preload and prefetch
- DNS prefetch and preconnect
- use the correct cache headers
- use Brotli or GZIP
- use responsive images
- use proper image formats
- compress images
- compress SVG
- compress CSS and Javascript
- subset fonts
- use WOFF2 fonts with WOFF fallback
- defer and lazy load scripts
- use lazy loading on images, video, iframes, etc
- use critical CSS
- use service workers for caching
- use save data hints
- use opacity, transform and will-change only on GPU accelerated animations
- use HTTP2
- use HPCACK
- use IPv6
- replace GIF with MP4

Their huge list of optimizations led them to their beloved 100% score. They proudly [presented](https://www.deleteagency.com/blog/how-to-get-a-100-percents-lighthouse-performance-score) their results and method on the International Search Summit, BrightonSEO and SearchLeeds. I was impressed and wrote [some nice words](/blog/how-to-get-a-100-google-lighthouse-score/) about their achievement.

## But then...

Only 18 months later their website scored a mediocre 42 out of 100 on the Google rank. That is because the score is relative, according to [Polly Pospelova](https://www.youtube.com/watch?v=4td0V4hUOww). She is partly right, as the performance score is a ranking. A perfect score means your website has the best performance of all tested websites. To be more [exact](https://web.dev/performance-scoring/): 'The scoring distribution is a log-normal distribution derived from the performance metrics of real website performance data on HTTP Archive'. This score is likely to change over time. A score of 50 means your website scores 'average'. However, a drop from 100 to 42 is unlikely to be due to this elastic measurement. It seems more like a case of poor maintenance.

## Perfect again

Just before Polly went to Digital Elite Camp 2019 in Estionia to talk about Lighthouse Scores, Delete Agency held a hackaton. They basically optimized 3 things:

- They started using HTTP2
- They optimize images
- They deferred the loading of non-critical resources

Those changes led them back to the perfect Lighthouse score. Polly proudly uploaded her presentation to [Youtube](https://www.youtube.com/watch?v=4td0V4hUOww) on april 2020. Her presentation contains some interesting topics and tips and is worth watching.

## History repeats

However, not a year after the upload the websites of Delete Agency is scoring 59 out of 100 again. Additionally the company has been taken over by (or 'became') [UNRVLD](https://unrvld.com/), which has a website that is scoring even worse.

## How to prevent this

Getting a perfect Lighthouse Score seems to be hard. Keeping a perfect Lighthouse Score seems even harder. But is it really? Because the websites that were build by me, which I [presented](/blog/how-to-get-a-100-google-lighthouse-score/) in november 2019, still score excellent. The trick is to not rely on optimizations, but simply on shipping less CSS and Javascript, which I explained in my post '[How to get a 100% Google Lighthouse score](/blog/how-to-get-a-100-google-lighthouse-score/))'. The current Delete Agency website (that is scoring 59 out of 100) is serving 200kb of CSS and 1500kb of Javascript. That is 13 times more CSS and 150 times more Javascript than this website. If they had rewritten their CSS and Javascript, instead of optimizing their load processes, their website would still have a perfect Lighthouse score today.

Do you want your website to have a perfect Google Lighthouse score too? [Contact me](/contact/)!