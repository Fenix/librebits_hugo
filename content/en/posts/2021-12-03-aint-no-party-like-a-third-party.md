---
title: Ain't no party like a third party
---

At the end of 2021 CSS tricks Chris Coyier asked web builders he admires: 'What is one thing people can do to make their website better?' I have highlighted two answers below.

*"Just take a minute to consider the implications of that: any third-party script on your site is allowing someone else to execute code on your web pages. That’s astonishingly unsafe."*

I like this one from [Jeremy Keith](https://css-tricks.com/aint-no-party-like-a-third-party/). He argues that removing third party scripts is hard, but my feeling is quite the opposite. I am an internet entrepreneur and I can choose any solution I like. My new hosting solution offers first party statistics and first party form handling. Therefore my average website does zero third party requests, which is not only good for speed and security, but also for GDPR compliancy. A win-win situation.

The next answer is from [Jake Albaugh](https://css-tricks.com/embrace-your-codes-transience/).

*"If you can justify the effort of writing your own code, you become more familiar with web specifications and learn just how robust they are on their own. You also end up with something that can be easier to maintain long-term because it is closest to the core evolutionary path of the web."*

He explains why code needs to change fairly often and why using dependencies is (opposed to what you would expect) counter-productive. Jake argues that, on the long run, it is more efficient to write your own code. 

The desire to use dependencies comes (in my opinion) from the assumption that the web is changing really fast and we need shortcuts. This is not true. The web barely changes at all. We can still run that [first CERN website](http://info.cern.ch/hypertext/WWW/TheProject.html) and upload it to any server via (S)FTP. It still works and is still as functional as it was back then, mainly because it has zero dependencies and does not use any third party solutions.

I agree with Jake that we should get more familiar with web specifications, as they change quite slowly (he calls them 'robust'). I refer to this thourough way of building and learning as [The Stackless Way](/blog/the-stackless-way).

What do you think? [What is one thing people can do to make their website better?](https://css-tricks.com/category/2021-end-of-year-thoughts/)
