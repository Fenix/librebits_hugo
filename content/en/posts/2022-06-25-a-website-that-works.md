---
title: Sometimes it should just work
---

I had to work on a clients Wordpress website and it was SLOOOOOWWWWW. You could get a fresh cup of coffee between page loads... well... almost. I asked this client why she would not use a cache plugin. 'We tried', she said... but all dynamic stuff broke when she turned it on. Obviously she did not know how to configure it correctly. Ouch... 

Sometimes you just want a website that works.

Then another client called, complaining about failing SSL certificate (re)generation. I immediately opened my terminal and logged in to my server through SSH to fix this, but found that this website was not hosted on our servers. It was on an external hosting platform. I would have fixed this in seconds, but now I had to access the server through a web interface. Unfortunately, the interface did not work correctly. I created a ticket and got an answer within a few hours: they would fix it. A day later it still was not fixed and the website was still down. Ouch... 

Sometimes all you want is a website that works.

Both situations illustrate what happens when you out-source parts of your website. As I wrote before: [You should not only master your software, you should master the system](/blog/code-warriors). All my newly created websites are built from scratch by me (no frameworks), are hosted by me and use a CMS built by me. I monitor the uptime at https://uptime.usecue.com/. I am on a journey of becoming a 'master of the system', a lifetime challenge.