---
title: The website obesity crisis
---

Websites get bigger every year. We can verify that by looking at the [state of the web by the HTTP Archive](https://httparchive.org/reports/state-of-the-web#bytesTotal). People like [Maciej Cegłowski](https://idlewords.com/talks/) and [Jeremy Keith](https://adactio.com) are pushing to fight this.

## Average versus median

An interesting thing is that the measurement changed [from the average page weight to the median page weight](https://www.igvita.com/2016/01/12/the-average-page-is-a-myth/) somewhere around 2017, which shifted the perception enormously. Prior to 2016 people talked about the average page size exceeding 3MB, while we are currently talking about median page weight exceeding 2MB. That is quite a difference. Nevertheless the growth is still worrysome and the numbers are increasing every year. 

## Some great examples

Maciej Cegłowski gives us some great examples in [his funny and passionate talk](https://idlewords.com/talks/website_obesity.htm) about website obesity he gave in 2015. I got inspired and tried to find some examples of my own. It was not very hard. I found an article titled ["The Website Obesity Crisis"](https://webdirections.org/blog/the-website-obesity-crisis/) that was weighing more than 3MB. I also found a [27MB page from Apple](https://www.apple.com/iphone-13/) about the iPhone 13. That last one is an unfair example, because Apple put some great video's on their iPhone 13 page, which obviously increase the size of the page. However, their [Terms of Service](https://www.apple.com/legal/internet-services/terms/site.html) page is also 2.3MB large (on mobile) and contains zero video's or images. Another great example is the 9MB weighing article ["It’s Time for a Global Green Certification for ICT"](https://www.engineeringforchange.org/news/time-global-green-certification-ict) from Mike Gifford.

In comparison, the page you are currently looking at is 0.04MB. This means that this page is 200 times lighter than Mike's article and 60 times lighter than Apple's Terms of Service. People seem to have forgotten how to render text on a page.

## Frameworks

The biggest reason for these bloated pages are frameworks. We even see this [obesity in the Jamstack](/blog/jamstack-means-performance-right). You have to look at frameworks as polyfills, [says Jeremy Keith](https://adactio.com/articles/18580) and I agree with him. A framework comes at a price, because a framework is heavy to load and it has a short life span. This means your website will perform poorly and you will have to keep learning a new one every year (or few years). 

This is why I have embraced [The Stackless Way](/blog/the-stackless-way/).