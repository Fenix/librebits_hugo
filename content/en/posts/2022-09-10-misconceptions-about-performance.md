---
title: Misconceptions about performance
---
There are a lot of misconceptions about website performance. I run into them on a daily basis. This time I found [an article on the awwwards website](https://www.awwwards.com/how-to-design-your-site-to-make-it-super-fast.html) about performance with a lot of misconceptions.

According to this article your design should be minimalistic. However, design and performance are barely related at all. I build perfectly scoring webpages on a daily basis without putting ANY restraint on the designers involved. This (strong) relation between design and performance is a misconception.

Typical is their advice to use a hamburger menu instead of a full menu. I guess it followed from the idea that hamburger menus are more minimalistic than full menus. Although that sounds logical, hamburger menus are not better for performance at all. If anything, a hamburger menu makes your website slower, because of the extra (JavaScript) code they require.

The awwwards article also points to the [Make the web faster](https://developers.google.com/speed/) from Google. The page seems to advert [a lot of useful tools](https://www.modpagespeed.com/doc/), but most things on that list will not increase, but decrease performance, so please watch out!

- *Inlining* CSS and JavaScript only helps above the fold and will generally slow down page loads when used elsewhere. 
- *Combining requests* (JavaScript or CSS) always slows down page loads over http/2, as multiple small requests are faster than one big one. 
- *Sprite images* have the same problem as combining requests.
- *Inlining images* is the worst thing I ever heard. This will make your image blocking during load, which makes your page MUCH slower. You should always avoid that.
- *Removing whitespace or minifying* when you can us gzipping? Not a (big) win.
- *Moving CSS to the head* is also questionable. You should load as little CSS in the head as possible, because CSS is blocking. Blocking CSS slows your website down.
- *Responsive images* are questionable when you can use the [one image technique](https://hugocodex.org/blog/image-compression-for-the-lazy/). It seems like an overkill and will not help significantly.
- *Loading Google Fonts* (externally) is a very bad idea, and it will slow your website down. 
- *Asyncing Google Analytics* is a simple script tag attribute, not a tool. However, I would not use Google Analytics at all.
- *Canonicalizing JavaScript libraries* causes an extra DNS lookup, which can take a lot of time.
- *Pre-resolving DNS lookups* only applies to third party requests. Third party requests are slow. Removing or asyncing them is the solution here. Prefetching DNS lookups will slow your website down.

I answered a Google survey (from C-space) where I was asked how I thought about Google helping us to build reliable websites faster. I reminded them that the only product from Google that did this (AMP), was [no success](https://plausible.io/blog/google-amp) at all. I answered that the proposition was 'very unbelievable' coming from Google. I think you can see why. Google is not very good at building these kind of products.

So... you should not listen to the people from awwwards, because ironically they don't know that much about website performance. Funny enough, you should also not (blindly) use the tools Google promotes on their [Make the web faster](https://developers.google.com/speed/) page. 

What SHOULD you do to increase performance? Well, that is easy. The only things you have to do are:

1. Use less CSS and JavaScript (no frameworks!)
1. Move CSS and JavaScript to the footer (if possible)
1. Gzip your request's responses
1. Use http/2
1. Use a local font (using font face)
1. Have decent image compression, using the [one image technique](https://hugocodex.org/blog/image-compression-for-the-lazy/)

That's all it takes to make your website load instantly. It is not as hard as it seems. You can do it too! Need help nevertheless? [Contact me!](/contact/)
