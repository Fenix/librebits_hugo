---
title: About me
---

My name is Fenix and I am a web developer based in Madrid. This harsh looking website is actually [a tribute to the web](/blog/a-tribute-to-the-web) i've borrowed from [Usecue 's website](https://usecue.com). 

I have studied Telecom, Sound, Image and Electronics Engineering in Barcelona. Founded 'Librebits' in 2012 in Buenos Aires. My first priority is my family, but my work is absolutely number two. I love what I do, which is building beautiful websites and web applications. 

All websites I build are well-coded and have good SEO. This is reflected by their excellent [Google score](/blog/google-lighthouse-score/), an objective quality measurement of a website. Quality is very important to me. I strongly believe that each job worth doing is worth doing well. Therefore I do not just build these websites, but host them as well. This supply chain integration is the reason I can deliver such high quality.

I am a big fan of Jekyll and Hugo (website generators). To help fellow enthusiasts, I collaborate on the [Hugo Codex](http://hugocodex.org) developement.

Apart from Hugo websites, I also develop custom web solutions.

&nbsp;

<p style="text-align: center;"><img src="/img/fenix-caricatura.jpeg" style="max-width: 100%; width: 400px;" alt="Fenix" /><br /><em>Me by a friend of mine</em></p>
