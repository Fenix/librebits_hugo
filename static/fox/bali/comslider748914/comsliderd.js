function comSlider748914() { 
var self = this; 
var g_HostRoot = "";
var g_TransitionTimeoutRef = null;
var g_CycleTimeout = 10;
var g_currFrame = 0;
var g_fontLoadJsRetries = 0;
var g_currDate = new Date(); var g_currTime = g_currDate.getTime();var g_microID = g_currTime + '-' + Math.floor((Math.random()*1000)+1); 
var g_InTransition = 0;var g_Navigation = 0;this.getCurrMicroID = function() { return g_microID; } 
var g_kb = new Array();
var g_kbsupported = true;
var isOldIE = navigator.userAgent.indexOf('MSIE 6')>=0 || navigator.userAgent.indexOf('MSIE 7')>=0 || navigator.userAgent.indexOf('MSIE 8')>=0;if (isOldIE) {g_kbsupported = false;}    this.kenburns = function(options) {     
								if (!g_kbsupported)
									return null; 				
                                var ctx = jqCS748914("#"+options.name)[0].getContext('2d');
                                var thisobj = this;

                                var start_time = 0;
                                                            //var width = $thiscanvas.width();
                                                            //var height = $thiscanvas.height();	
                                                            var width = options.width;
                                                            var height = options.height;	


                                var image_path = options.image;		
                                var display_time = options.display_time || 7000;
                                var fade_time = options.fade_time || 0;
                                var fade_called = false;
                                var frames_per_second = options.frames_per_second || 30;		
                                var frame_time = (1 / frames_per_second) * 1000;
                                var zoom_level = 1 / (options.zoom || 2);
                                var clear_color = options.background_color || '#000000';	

                                var onstop = null;
                                var onloaded = null;
                                var onfade = null;

                                var timer_ref = null;
                                var images = [];
															
                                images.push({path:image_path,
                                                        initialized:false,
                                                        loaded:false});
                                function get_time() {
                                        var d = new Date();
                                        return d.getTime() - start_time;
                                }

                                function interpolate_point(x1, y1, x2, y2, i) {
                                        // Finds a point between two other points
                                        return  {x: x1 + (x2 - x1) * i,
                                                        y: y1 + (y2 - y1) * i}
                                }

                                function interpolate_rect(r1, r2, i) {
                                        // Blend one rect in to another
                                        var p1 = interpolate_point(r1[0], r1[1], r2[0], r2[1], i);
                                        var p2 = interpolate_point(r1[2], r1[3], r2[2], r2[3], i);
                                        return [p1.x, p1.y, p2.x, p2.y];
                                }

                                function scale_rect(r, scale) {
                                        // Scale a rect around its center
                                        var w = r[2] - r[0];
                                        var h = r[3] - r[1];
                                        var cx = (r[2] + r[0]) / 2;
                                        var cy = (r[3] + r[1]) / 2;
                                        var scalew = w * scale;
                                        var scaleh = h * scale;
                                        return [cx - scalew/2,
                                                        cy - scaleh/2,
                                                        cx + scalew/2,
                                                        cy + scaleh/2];		
                                }

                                function fit(src_w, src_h, dst_w, dst_h) {
                                        // Finds the best-fit rect so that the destination can be covered
                                        var src_a = src_w / src_h;
                                        var dst_a = dst_w / dst_h;			
                                        var w = src_h * dst_a;
                                        var h = src_h;						
                                        if (w > src_w)
                                        {
                                                var w = src_w;
                                                var h = src_w / dst_a;
                                        }						
                                        var x = (src_w - w) / 2;
                                        var y = (src_h - h) / 2;
                                        return [x, y, x+w, y+h]; 
                                }				

                                function get_image_info() {
                                        // Gets information structure for a given index
                                        // Also loads the image asynchronously, if required		
                                        var image_info = images[0];
                                        if (!image_info.initialized) {
                                                var image = new Image();
                                                image_info.image = image;
                                                image_info.loaded = false;
                                                image.onload = function(){
                                                        image_info.loaded = true;
                                                        var iw = image.width;
                                                        var ih = image.height;

                                                        var r1 = fit(iw, ih, width, height);;
                                                        var r2 = scale_rect(r1, zoom_level);

                                                        var align_x = Math.floor(Math.random() * 3) - 1;
                                                        var align_y = Math.floor(Math.random() * 3) - 1;
                                                        align_x /= 2;
                                                        align_y /= 2;

                                                        var x = r2[0];
                                                        r2[0] += x * align_x;
                                                        r2[2] += x * align_x; 

                                                        var y = r2[1];
                                                        r2[1] += y * align_y;
                                                        r2[3] += y * align_y;
											
												if (Math.floor((Math.random()*10)) % 2) {
														image_info.r1 = r1;
														image_info.r2 = r2;
												}
												else {
														image_info.r1 = r2;
														image_info.r2 = r1;
												}												
															       if (options.onloaded) {
                                                                options.onloaded(thisobj);
                                                        }					

                                                }				
                                                image_info.initialized = true;
                                                image.src = image_info.path;
                                        }
                                        return image_info;
                                }

                                function render_image(image_index, anim) {
                                        // Renders a frame of the effect	
                                        if (anim > 1) {
                                                return;
                                        } 									
                                        var image_info = get_image_info();
                                        if (image_info.loaded) {						
                                                var r = interpolate_rect(image_info.r1, image_info.r2, anim);

                                                ctx.save();
                                                ctx.globalAlpha = 1;
                                                ctx.drawImage(image_info.image, r[0], r[1], r[2] - r[0], r[3] - r[1], 0, 0, width, height);
                                                ctx.restore();

                                        }
                                }				

                                function clear() {
                                        // Clear the canvas
                                        ctx.save();
                                        ctx.globalAlpha = 1;
                                        ctx.fillStyle = clear_color;
                                        ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
                                        ctx.restore();
                                }


                                function update() {

                                        // Render the next frame										
                                        var time_passed = get_time();	

                                        render_image(0, time_passed / display_time/*, time_passed / fade_time*/);			

                                        if ((fade_time > 0) && (fade_called == false) && ((display_time - time_passed) <= fade_time))
                                        {
                                                if (options.onfade) {
                                                        options.onfade(thisobj, display_time - time_passed);	
                                                }					
                                                fade_called = true;					
                                        }

                                        if (time_passed >= display_time)
                                        {
                                                thisobj.stop();
                                                return;
                                        }
                                }

                                this.stop = function()
                                {
                                        if (timer_ref != null)
                                                clearInterval(timer_ref);
                                        timer_ref = null;
                                        //clear();
                                        images[0].initialized = null;			
                                        if (options.onstop) {
                                                options.onstop(thisobj);
                                        }
                                }

                                this.start = function()
                                {
                                        fade_called = false;		
                                        start_time = 0;
                                        start_time = get_time();	
                                        timer_ref = setInterval(update, frame_time);	
                                }

                                get_image_info();	
                                return this;	
                        }	
               this.setNavStyle = function(id, background, color, border, type)
{
 if (background == "")
 {
     jqCS748914("#comSNavigation748914_"+id).css("background", "none");
 }
 else if (background == "transparent")
 {
     jqCS748914("#comSNavigation748914_"+id).css("background", "transparent");
 }
 else
 {
     jqCS748914("#comSNavigation748914_"+id).css("background", "#"+background);
 }
 jqCS748914("#comSNavigation748914_"+id).css("color", "#"+color);
 if (background == "transparent") { jqCS748914("#comSNavigation748914_"+id).css("border", border+"px solid transparent"); } else if (background != "") { jqCS748914("#comSNavigation748914_"+id).css("border", border+"px solid #"+background); } else { jqCS748914("#comSNavigation748914_"+id).css("border", border+"px"); } 
 var margin = (-1)*border;
 jqCS748914("#comSNavigation748914_"+id).css("margin-top", margin+"px");
 jqCS748914("#comSNavigation748914_"+id).css("margin-left", margin+"px");
 if (type == 0)
 {
   jqCS748914("#comImgBullet748914_"+id).show();
   jqCS748914("#comImgBulletcurr748914_"+id).hide();
 }
 else
 {
   jqCS748914("#comImgBulletcurr748914_"+id).show();
   jqCS748914("#comImgBullet748914_"+id).hide();
 }
}
this.targetClearTimeouts = function()
{
 if (g_TransitionTimeoutRef != null)     { window.clearTimeout(g_TransitionTimeoutRef); g_TransitionTimeoutRef = null;}
}
this.getNextFrame = function()
{
 var ret = g_currFrame;
 ret++;
 if (ret == 4) {ret = 0;}
 return ret;
}
this.getPrevFrame = function()
{
 var ret = g_currFrame;
 ret--;
 if (ret < 0) {ret = (4-1);}
 return ret;
}
this.stopAll = function()
{
jqCS748914("#comSFrame748914_0").stop(true, true);
jqCS748914("#comSFrameSek748914_0").stop(true, true);
jqCS748914("#comSFrame748914_1").stop(true, true);
jqCS748914("#comSFrameSek748914_1").stop(true, true);
jqCS748914("#comSFrame748914_2").stop(true, true);
jqCS748914("#comSFrameSek748914_2").stop(true, true);
jqCS748914("#comSFrame748914_3").stop(true, true);
jqCS748914("#comSFrameSek748914_3").stop(true, true);
}
this.switchFrame = function()
{
     g_Navigation = 1;
     var currFrame=g_currFrame;
     g_currFrame = self.getNextFrame();
     self.switchFromToFrame(currFrame, g_currFrame);
}
 
this.switchFramePrev = function()
{
     g_Navigation = 0;
     var currFrame=g_currFrame;
     g_currFrame = self.getPrevFrame();
     self.switchFromToFrame(currFrame, g_currFrame);
}
 
this.switchToFrame = function(toFrame)
{
     if ((g_InTransition == 1) || (g_currFrame == toFrame))
     {
         if (g_currFrame == toFrame) { return false; }
         self.stopAll();
     }
     var currFrame=g_currFrame;
     g_currFrame=toFrame;
     if (currFrame < g_currFrame)
         g_Navigation = 0;
     else
         g_Navigation = 1;
     self.switchFromToFrame(currFrame, g_currFrame);
}
 
this.switchFromToFrame =  function(currFrame, toFrame)
{
     if (g_InTransition == 1)
     {
         self.stopAll();
     }
g_InTransition = 1;
self.startTransitionTimer();
if (g_kb.length > toFrame)
	g_kb[toFrame].start();
     jqCS748914("#comSFrameSek748914_"+currFrame+"").css("z-index", 1);
     jqCS748914("#comSFrameSek748914_"+toFrame+"").css("z-index", 2);
     jqCS748914("#comSFrameSek748914_"+toFrame+"").hide().fadeIn(2500, function() { 
if (g_microID !=objcomSlider748914.getCurrMicroID()){return false;};jqCS748914("#comSFrame748914_"+currFrame).hide(); g_InTransition = 0;
 } ); 
  self.setNavStyle(currFrame, 'transparent','FFFFFF',0, 0);  self.setNavStyle(toFrame, 'transparent','C3419C',0, 1);     jqCS748914("#comSFrame748914_"+toFrame).show(1, function(){  });
if (g_kb.length > currFrame)
	g_kb[currFrame].stop();
     
     
     
     
}
this.startTransitionTimer = function()
{
  self.targetClearTimeouts(); g_TransitionTimeoutRef = window.setTimeout(function() {objcomSlider748914.onTransitionTimeout(g_microID)}, g_CycleTimeout*1000);
}
this.onTransitionTimeout = function(microID)
{
   if (g_microID != microID) { return false; }
     self.switchFrame();
}
this.initFrame = function()
{
g_currFrame = 0;
self.startTransitionTimer();
if (g_kb.length)
    g_kb[0].start();
  jqCS748914("#comSFrame748914_"+g_currFrame).show(1, function(){if (g_microID !=objcomSlider748914.getCurrMicroID()){return false;};self.setNavStyle(g_currFrame, 'transparent','C3419C',0, 1);     });
  return true;
}

					this.scriptLoaded = function()
					{
				   jqCS748914 = jQuery748914.noConflict(false);jqCS748914("#comslider_in_point_748914").html('<div id="comSWrapper748914_" name="comSWrapper748914_" style="display: inline-block; text-align: center;overflow:hidden;border:0px; width:560px; height:384px; position: relative;"><div id="comSFrameWrapper748914_" name="comSFrameWrapper748914_" style="position: absolute; top: 0px; left: 0px;"><div id="comSFrame748914_0" name="comSFrame748914_0" style="position:absolute; top:0px; left:0px; width:560px; height:384px;"><div id="comSFrameSek748914_0" name="comSFrameSek748914_0" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><div id="comSImg748914_0" name="comSImg748914_0" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><canvas id="kenburns748914_0" width="560" height="384"></canvas></div></div></div><div id="comSFrame748914_1" name="comSFrame748914_1" style="position:absolute; top:0px; left:0px; width:560px; height:384px;"><div id="comSFrameSek748914_1" name="comSFrameSek748914_1" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><div id="comSImg748914_1" name="comSImg748914_1" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><canvas id="kenburns748914_1" width="560" height="384"></canvas></div></div></div><div id="comSFrame748914_2" name="comSFrame748914_2" style="position:absolute; top:0px; left:0px; width:560px; height:384px;"><div id="comSFrameSek748914_2" name="comSFrameSek748914_2" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><div id="comSImg748914_2" name="comSImg748914_2" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><canvas id="kenburns748914_2" width="560" height="384"></canvas></div></div></div><div id="comSFrame748914_3" name="comSFrame748914_3" style="position:absolute; top:0px; left:0px; width:560px; height:384px;"><div id="comSFrameSek748914_3" name="comSFrameSek748914_3" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><div id="comSImg748914_3" name="comSImg748914_3" style="position:absolute; overflow:hidden; top:0px; left:0px; width:560px; height:384px;"><canvas id="kenburns748914_3" width="560" height="384"></canvas></div></div></div></div><a name="0" style="cursor:pointer; text-decoration:none !important; font-size:16px;" href=""><div id="comSNavigation748914_0" name="comSNavigation748914_0" style="border-radius: 40px; margin-left:0px; margin-top:0px; border: 0px solid transparent; position:absolute; height:20px; width:20px; top:359px; left:5px; z-index: 5; text-align: center; vertical-align:bottom;  color: #FFFFFF;background: transparent; "><div id="height_workaround" style="font-size:1px;line-height:0;height:20px;">&nbsp;<img style="position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBullet748914_0" name="comImgBullet748914_0" src="comslider748914/imgnav/nav2.png?timstamp=1440763721" /><img style="display: none; position: absolute; position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBulletcurr748914_0" name="comImgBulletcurr748914_0" src="comslider748914/imgnav/navs2.png?timstamp=1440763721" /></div></div></a><a name="1" style="cursor:pointer; text-decoration:none !important; font-size:16px;" href=""><div id="comSNavigation748914_1" name="comSNavigation748914_1" style="border-radius: 40px; margin-left:0px; margin-top:0px; border: 0px solid transparent; position:absolute; height:20px; width:20px; top:359px; left:30px; z-index: 5; text-align: center; vertical-align:bottom;  color: #FFFFFF;background: transparent; "><div id="height_workaround" style="font-size:1px;line-height:0;height:20px;">&nbsp;<img style="position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBullet748914_1" name="comImgBullet748914_1" src="comslider748914/imgnav/nav2.png?timstamp=1440763721" /><img style="display: none; position: absolute; position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBulletcurr748914_1" name="comImgBulletcurr748914_1" src="comslider748914/imgnav/navs2.png?timstamp=1440763721" /></div></div></a><a name="2" style="cursor:pointer; text-decoration:none !important; font-size:16px;" href=""><div id="comSNavigation748914_2" name="comSNavigation748914_2" style="border-radius: 40px; margin-left:0px; margin-top:0px; border: 0px solid transparent; position:absolute; height:20px; width:20px; top:359px; left:55px; z-index: 5; text-align: center; vertical-align:bottom;  color: #FFFFFF;background: transparent; "><div id="height_workaround" style="font-size:1px;line-height:0;height:20px;">&nbsp;<img style="position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBullet748914_2" name="comImgBullet748914_2" src="comslider748914/imgnav/nav2.png?timstamp=1440763721" /><img style="display: none; position: absolute; position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBulletcurr748914_2" name="comImgBulletcurr748914_2" src="comslider748914/imgnav/navs2.png?timstamp=1440763721" /></div></div></a><a name="3" style="cursor:pointer; text-decoration:none !important; font-size:16px;" href=""><div id="comSNavigation748914_3" name="comSNavigation748914_3" style="border-radius: 40px; margin-left:0px; margin-top:0px; border: 0px solid transparent; position:absolute; height:20px; width:20px; top:359px; left:80px; z-index: 5; text-align: center; vertical-align:bottom;  color: #FFFFFF;background: transparent; "><div id="height_workaround" style="font-size:1px;line-height:0;height:20px;">&nbsp;<img style="position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBullet748914_3" name="comImgBullet748914_3" src="comslider748914/imgnav/nav2.png?timstamp=1440763721" /><img style="display: none; position: absolute; position: absolute; top: 0px; left: 0px; border:0px;" id="comImgBulletcurr748914_3" name="comImgBulletcurr748914_3" src="comslider748914/imgnav/navs2.png?timstamp=1440763721" /></div></div></a></div>');
                    jqCS748914("#comslider_in_point_748914 a").bind('click',  function() { if ((this.name.length > 0) && (isNaN(this.name) == false)) {  self.switchToFrame(parseInt(this.name)); return false;} });
                
				
						if (g_kbsupported == true)						
						{				
							g_kb[0] = new self.kenburns({
									name: 'kenburns748914_0',
									width: 560,
									height: 384,
image:'comslider748914/img/1508281300385301.jpg?1440759930',
     frames_per_second: 30,
									display_time: 10000, 
									fade_time: 0,
									zoom: 1.5,
									background_color:'#ffffff',
									onstop:function(kenburnsobj) { },
									onloaded:function(kenburnsobj) { },
									onfade:function(kenburnsobj, timeleft) { }
							});						
						}
						else
						{
							jqCS748914("#comSImg748914_0").html('<img src="http://commondatastorage.googleapis.com/comslider/target/users/1440759610x301c6645db6653e18fc340998cba4fee/img/1508281300385301.jpg?1440759930"/>');						
						}

				
						if (g_kbsupported == true)						
						{				
							g_kb[1] = new self.kenburns({
									name: 'kenburns748914_1',
									width: 560,
									height: 384,
image:'comslider748914/img/1508281300118191.jpg?1440759930',
     frames_per_second: 30,
									display_time: 10000, 
									fade_time: 0,
									zoom: 1.5,
									background_color:'#ffffff',
									onstop:function(kenburnsobj) { },
									onloaded:function(kenburnsobj) { },
									onfade:function(kenburnsobj, timeleft) { }
							});						
						}
						else
						{
							jqCS748914("#comSImg748914_1").html('<img src="http://commondatastorage.googleapis.com/comslider/target/users/1440759610x301c6645db6653e18fc340998cba4fee/img/1508281300118191.jpg?1440759930"/>');						
						}
jqCS748914("#comSFrame748914_1").hide();

				
						if (g_kbsupported == true)						
						{				
							g_kb[2] = new self.kenburns({
									name: 'kenburns748914_2',
									width: 560,
									height: 384,
image:'comslider748914/img/1508281300201201.jpg?1440759930',
     frames_per_second: 30,
									display_time: 10000, 
									fade_time: 0,
									zoom: 1.5,
									background_color:'#ffffff',
									onstop:function(kenburnsobj) { },
									onloaded:function(kenburnsobj) { },
									onfade:function(kenburnsobj, timeleft) { }
							});						
						}
						else
						{
							jqCS748914("#comSImg748914_2").html('<img src="http://commondatastorage.googleapis.com/comslider/target/users/1440759610x301c6645db6653e18fc340998cba4fee/img/1508281300201201.jpg?1440759930"/>');						
						}
jqCS748914("#comSFrame748914_2").hide();

				
						if (g_kbsupported == true)						
						{				
							g_kb[3] = new self.kenburns({
									name: 'kenburns748914_3',
									width: 560,
									height: 384,
image:'comslider748914/img/1508281300288961.jpg?1440759930',
     frames_per_second: 30,
									display_time: 10000, 
									fade_time: 0,
									zoom: 1.5,
									background_color:'#ffffff',
									onstop:function(kenburnsobj) { },
									onloaded:function(kenburnsobj) { },
									onfade:function(kenburnsobj, timeleft) { }
							});						
						}
						else
						{
							jqCS748914("#comSImg748914_3").html('<img src="http://commondatastorage.googleapis.com/comslider/target/users/1440759610x301c6645db6653e18fc340998cba4fee/img/1508281300288961.jpg?1440759930"/>');						
						}
jqCS748914("#comSFrame748914_3").hide();
self.initFrame();

}
var g_CSIncludes = new Array();
var g_CSLoading = false;
var g_CSCurrIdx = 0; 
 this.include = function(src, last) 
                {
                    if (src != '')
                    {				
                            var tmpInclude = Array();
                            tmpInclude[0] = src;
                            tmpInclude[1] = last;					
                            //
                            g_CSIncludes[g_CSIncludes.length] = tmpInclude;
                    }            
                    if ((g_CSLoading == false) && (g_CSCurrIdx < g_CSIncludes.length))
                    {


                            var oScript = null;
                            if (g_CSIncludes[g_CSCurrIdx][0].split('.').pop() == 'css')
                            {
                                oScript = document.createElement('link');
                                oScript.href = g_CSIncludes[g_CSCurrIdx][0];
                                oScript.type = 'text/css';
                                oScript.rel = 'stylesheet';

                                oScript.onloadDone = true; 
                                g_CSLoading = false;
                                g_CSCurrIdx++;								
                                if (g_CSIncludes[g_CSCurrIdx-1][1] == true) 
                                        self.scriptLoaded(); 
                                else
                                        self.include('', false);
                            }
                            else
                            {
                                oScript = document.createElement('script');
                                oScript.src = g_CSIncludes[g_CSCurrIdx][0];
                                oScript.type = 'text/javascript';

                                //oScript.onload = scriptLoaded;
                                oScript.onload = function() 
                                { 
                                        if ( ! oScript.onloadDone ) 
                                        {
                                                oScript.onloadDone = true; 
                                                g_CSLoading = false;
                                                g_CSCurrIdx++;								
                                                if (g_CSIncludes[g_CSCurrIdx-1][1] == true) 
                                                        self.scriptLoaded(); 
                                                else
                                                        self.include('', false);
                                        }
                                };
                                oScript.onreadystatechange = function() 
                                { 
                                        if ( ( "loaded" === oScript.readyState || "complete" === oScript.readyState ) && ! oScript.onloadDone ) 
                                        {
                                                oScript.onloadDone = true;
                                                g_CSLoading = false;	
                                                g_CSCurrIdx++;
                                                if (g_CSIncludes[g_CSCurrIdx-1][1] == true) 
                                                        self.scriptLoaded(); 
                                                else
                                                        self.include('', false);
                                        }
                                }
                                
                            }
                            //
                            document.getElementsByTagName("head").item(0).appendChild(oScript);
                            //
                            g_CSLoading = true;
                    }

                }
                

}
objcomSlider748914 = new comSlider748914();
objcomSlider748914.include('comslider748914/js/helpers.js', false);
objcomSlider748914.include('comslider748914/js/jquery-1.10.1.js', false);
objcomSlider748914.include('comslider748914/js/jquery-ui-1.10.3.effects.js', true);
